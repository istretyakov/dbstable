﻿using System;
using System.Windows;
using System.Windows.Controls;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class LessonWindow : Window
    {
        private UserModel learnerUser = null;
        private UserModel coachUser = null;
        private HorseModel horseHorse = null;
        private int chosenType;

        public LessonModel CurrentLesson { get; set; } = null;  // null - создание,
                                                                // не null - редактирование

        public DateTime ChosenDate { get; set; }
        public HorseModel ChosenHorse { get; set; } = null;
        public int ChosenHour { get; set; } = 0;

        public LessonWindow()
        {
            InitializeComponent();

            chosenType = 0;

            this.Loaded += LessonWindow_Loaded;
        }

        private void LessonWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (CurrentLesson == null)
            {
                ChosenDate = ChosenDate.AddHours(-ChosenDate.Hour);
                ChosenDate = ChosenDate.AddMinutes(-ChosenDate.Minute);
                ChosenDate = ChosenDate.AddSeconds(-ChosenDate.Second);
                DateDatePicker.SelectedDate = ChosenDate;

                if (ChosenHorse != null)
                    GiveHorseModel(ChosenHorse);
                if (ChosenHour >= 8 && ChosenHour <= 20)
                    StartHourComboBox.SelectedIndex = ChosenHour - 8;
            }
            else
            {
                CurrentLesson = Database.LoadLesson(CurrentLesson.Id);

                DateTime dateTime = CurrentLesson.StartTime;
                dateTime = dateTime.AddHours(-dateTime.Hour);
                dateTime = dateTime.AddMinutes(-dateTime.Minute);
                dateTime = dateTime.AddSeconds(-dateTime.Second);
                DateDatePicker.SelectedDate = dateTime;

                StartHourComboBox.SelectedIndex = CurrentLesson.StartTime.Hour - 8;
                FinishHourComboBox.SelectedIndex = StartHourComboBox.SelectedIndex + 1;

                learnerUser = Database.LoadUser(CurrentLesson.LearnerId);
                LearnerNameTextBlock.Text = learnerUser.Name;
                coachUser = Database.LoadUser(CurrentLesson.CoachId);
                CoachNameTextBlock.Text = coachUser.Name;
                horseHorse = Database.LoadHorse(CurrentLesson.HorseId);
                HorseNameTextBlock.Text = horseHorse.Name;

                if (CurrentLesson.Type == 0)    // Разовое занятие
                {
                    LessonTypeCheckBox.IsChecked = false;
                    PaymentModel incomeModel = Database.LoadPayment(CurrentLesson.IncomeId);
                    if (incomeModel != null)
                    {
                        CostTextBox.Text = $"{incomeModel.Value}";
                        if (incomeModel.Type != PaymentTypes.NotPaid)
                            PaidCheckBox.IsChecked = true;
                        else
                            PaidCheckBox.IsChecked = false;
                    }
                }
                else
                    LessonTypeCheckBox.IsChecked = true;

                PaymentModel salaryModel = Database.LoadPayment(CurrentLesson.SalaryId);
                if (salaryModel != null)
                    SalaryAmountTextBox.Text = $"{salaryModel.Value}";
            }
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            LessonModel lessonModel;
            PaymentModel salaryModel;
            PaymentModel incomeModel;
            int result = GetFormInformation(out lessonModel, out salaryModel, out incomeModel);

            switch (result)
            {
                case 1:
                    MessageBox.Show("Вы ввели недопустимую сумму зарплаты");
                    return;

                case 2:
                    MessageBox.Show("Вы ввели недопустимую стоимость занятия");
                    return;

                case 3:
                    MessageBox.Show("Вы ввели недопустимую стоимость занятия");
                    return;

                case 4:
                    MessageBox.Show("Вы ввели недопустимую сумму зарплаты");
                    return;

                case 5:
                    MessageBox.Show("Вы не выбрали всадника");
                    return;

                case 6:
                    MessageBox.Show("Вы не выбрали тренера");
                    return;

                case 7:
                    MessageBox.Show("Вы не выбрали коня");
                    return;
            }

            if (lessonModel != null && incomeModel != null && salaryModel != null)
            {
                if (Database.DoesLessonExist(horseHorse.Id, lessonModel.StartTime))
                {
                    MessageBox.Show("Выбранный конь в это время занят");
                    return;
                }

                Database.CreatePayment(salaryModel);
                PaymentModel cSalaryModel = Database.LoadLastPayment();
                lessonModel.SalaryId = cSalaryModel.Id;

                Database.CreatePayment(incomeModel);
                PaymentModel cIncomeModel = Database.LoadLastPayment();
                lessonModel.IncomeId = cIncomeModel.Id;
                
                Database.CreateLesson(lessonModel);

                MessageBox.Show("Вы успешно создали тренировку");

                DayWindow dayWindow = this.Owner as DayWindow;
                if (dayWindow != null)
                    dayWindow.ShowLessons(ChosenDate);

                this.Close();
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ChooseLearnerButton_Click(object sender, RoutedEventArgs e)
        {
            chosenType = 0;

            UsersWindow usersWindow = new UsersWindow();
            usersWindow.Owner = this;
            usersWindow.SetTypeUsersWindow(1);
            usersWindow.ShowDialog();
        }

        private void ChooseCoachButton_Click(object sender, RoutedEventArgs e)
        {
            chosenType = 1;

            UsersWindow usersWindow = new UsersWindow();
            usersWindow.Owner = this;
            usersWindow.SetTypeUsersWindow(1);
            usersWindow.ShowDialog();
        }

        private void ChooseHorseButton_Click(object sender, RoutedEventArgs e)
        {
            HorsesWindow horsesWindow = new HorsesWindow();
            horsesWindow.Owner = this;
            horsesWindow.SetTypeHorseWindow(1);
            horsesWindow.ShowDialog();
        }

        public void GiveUserModel(UserModel userModel)
        {
            if (chosenType == 0)
            {
                learnerUser = userModel;
                LearnerNameTextBlock.Text = learnerUser.Name;
            }
            else if (chosenType == 1)
            {
                coachUser = userModel;
                CoachNameTextBlock.Text = coachUser.Name;
            }
        }

        public void GiveHorseModel(HorseModel horseModel)
        {
            horseHorse = horseModel;
            HorseNameTextBlock.Text = horseHorse.Name;
        }

        private void StartHourComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FinishHourComboBox != null && StartHourComboBox != null)
                FinishHourComboBox.SelectedIndex = StartHourComboBox.SelectedIndex + 1;
        }

        private void LessonTypeCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (LessonTypeCheckBox.IsChecked == true)
                LessonTypeCheckBox.Content = "По абонементу";
            else
                LessonTypeCheckBox.Content = "Разовое занятие";
        }

        private void OtherComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            if (comboBox != null)
            {
                if (comboBox == FinishHourComboBox)
                    comboBox.SelectedIndex = StartHourComboBox.SelectedIndex + 1;
                else
                    comboBox.SelectedIndex = 0;
            }
        }

        private void EditLessonButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentLesson != null)
            {
                LessonModel lessonModel;
                PaymentModel salaryModel;
                PaymentModel incomeModel;
                int result = GetFormInformation(out lessonModel, out salaryModel, out incomeModel);

                switch (result)
                {
                    case 1:
                        MessageBox.Show("Вы ввели недопустимую сумму зарплаты");
                        return;

                    case 2:
                        MessageBox.Show("Вы ввели недопустимую стоимость занятия");
                        return;

                    case 3:
                        MessageBox.Show("Вы ввели недопустимую стоимость занятия");
                        return;

                    case 4:
                        MessageBox.Show("Вы ввели недопустимую сумму зарплаты");
                        return;

                    case 5:
                        MessageBox.Show("Вы не выбрали всадника");
                        return;

                    case 6:
                        MessageBox.Show("Вы не выбрали тренера");
                        return;

                    case 7:
                        MessageBox.Show("Вы не выбрали коня");
                        return;
                }

                if (lessonModel != null && incomeModel != null && salaryModel != null)
                {
                    lessonModel.Id = CurrentLesson.Id;
                    incomeModel.Id = CurrentLesson.IncomeId;
                    salaryModel.Id = CurrentLesson.SalaryId;

                    lessonModel.IncomeId = incomeModel.Id;
                    lessonModel.SalaryId = salaryModel.Id;

                    Database.SaveLesson(lessonModel);
                    Database.SavePayment(incomeModel);
                    Database.SavePayment(salaryModel);

                    MessageBox.Show("Вы успешно отредактировали тренировку");

                    DayWindow dayWindow = this.Owner as DayWindow;
                    if (dayWindow != null)
                        dayWindow.ShowLessons(lessonModel.StartTime);
                    else
                    {
                        EditUserWindow editUserWindow = this.Owner as EditUserWindow;
                        if (editUserWindow != null)
                            editUserWindow.ShowLessons();
                    }
                }
            }
        }

        private void DeleteLessonButton_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentLesson != null)
            {
                MessageBoxResult result = MessageBox.Show("Вы уверены, что хотите удалить тренировку?",
                    "Удаление тренировки", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.Yes)
                {
                    Database.DeletePayment(CurrentLesson.IncomeId);
                    Database.DeletePayment(CurrentLesson.SalaryId);
                    Database.DeleteLesson(CurrentLesson.Id);

                    MessageBox.Show("Вы успешно удалили тренировку");

                    DayWindow dayWindow = this.Owner as DayWindow;
                    if (dayWindow != null)
                        dayWindow.ShowLessons(CurrentLesson.StartTime);
                    else
                    {
                        EditUserWindow editUserWindow = this.Owner as EditUserWindow;
                        if (editUserWindow != null)
                            editUserWindow.ShowLessons();
                    }
                    this.Close();
                }
            }
        }

        public void SetType(int type)
        {
            if (type == 0)
            {
                CreateButton.Margin = new Thickness(0, 0, 10, 0);
                CreateButton.Width = 70;
                CreateButton.Visibility = Visibility.Visible;

                EditButton.Margin = new Thickness(0, 0, 0, 0);
                EditButton.Width = 0;
                EditButton.Visibility = Visibility.Hidden;

                DeleteButton.Margin = new Thickness(0, 0, 0, 0);
                DeleteButton.Width = 0;
                DeleteButton.Visibility = Visibility.Hidden;
            }
            else if (type == 1)
            {
                CreateButton.Margin = new Thickness(0, 0, 0, 0);
                CreateButton.Width = 0;
                CreateButton.Visibility = Visibility.Hidden;

                EditButton.Margin = new Thickness(0, 0, 10, 0);
                EditButton.Width = 70;
                EditButton.Visibility = Visibility.Visible;

                DeleteButton.Margin = new Thickness(0, 0, 10, 0);
                DeleteButton.Width = 70;
                DeleteButton.Visibility = Visibility.Visible;
            }
        }

        private int GetFormInformation(out LessonModel lessonModel, out PaymentModel salaryModel, out PaymentModel incomeModel)
        {
            lessonModel = null;
            salaryModel = null;
            incomeModel = null;

            ComboBoxItem item = StartHourComboBox.SelectedItem as ComboBoxItem;
            int cost = 0;
            int salaryAmount;
            bool result;
            DateTime selectedDateTime = DateDatePicker.SelectedDate.Value.AddHours(Convert.ToInt32(item.Content));

            result = int.TryParse(SalaryAmountTextBox.Text, out salaryAmount);
            if (!result) // Недопустимая сумма зарплаты
                return 1;
            if (LessonTypeCheckBox.IsChecked == false)
            {
                result = int.TryParse(CostTextBox.Text, out cost);
                if (!result) // Недопустимая стоимость занятия
                    return 2;
                if (cost < 0 || cost > 100000) // Недопустима стоимость занятия
                    return 3;
            }
            if (salaryAmount < 0 || salaryAmount > 100000) // Недопустимая сумма зарплаты
                return 4;
            if (learnerUser == null) // Не выбран всадник
                return 5;
            if (coachUser == null) // Не выбран тренер
                return 6;
            if (horseHorse == null) // Не выбран конь
                return 7;

            lessonModel = new LessonModel();
            lessonModel.CoachId = coachUser.Id;
            lessonModel.LearnerId = learnerUser.Id;
            lessonModel.HorseId = horseHorse.Id;
            lessonModel.StartTime = selectedDateTime;
            lessonModel.FinishTime = lessonModel.StartTime.AddHours(1);
            if (LessonTypeCheckBox.IsChecked == false)
                lessonModel.Type = 0;
            else
                lessonModel.Type = 1;
            lessonModel.IncomeId = 0;
            lessonModel.SalaryId = 0;

            salaryModel = new PaymentModel();
            salaryModel.UserId = coachUser.Id;
            salaryModel.Value = salaryAmount;
            salaryModel.Date = lessonModel.StartTime;
            salaryModel.Type = PaymentTypes.Salary;
            
            incomeModel = new PaymentModel();
            incomeModel.UserId = learnerUser.Id;
            incomeModel.Date = lessonModel.StartTime;
            if (LessonTypeCheckBox.IsChecked == false)
            {
                incomeModel.Value = cost;
                if (PaidCheckBox.IsChecked == true)
                    incomeModel.Type = PaymentTypes.OneTime;
                else
                    incomeModel.Type = PaymentTypes.NotPaid;
            }
            else
            {
                incomeModel.Type = PaymentTypes.Ticket;
                incomeModel.Value = 1500;
            }

            return 0;
        }
    }
}