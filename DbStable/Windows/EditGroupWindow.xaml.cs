﻿using System.Windows;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class EditGroupWindow : Window
    {
        private GroupModel myGroupModel;

        public EditGroupWindow()
        {
            InitializeComponent();
        }

        private void SaveGroupButton_Click(object sender, RoutedEventArgs e)
        {
            string name = NameTextBox.Text;

            myGroupModel.Name = name;
            Database.SaveGroup(myGroupModel);

            MessageBox.Show($"Вы сохранили группу {myGroupModel.Name}");

            GroupsWindow groupsWindow = this.Owner as GroupsWindow;

            this.Close();

            if (groupsWindow != null)
                groupsWindow.UpdatePage();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void SetInfo(string name)
        {
            GroupModel groupModel = Database.LoadGroup(name);

            NameTextBox.Text = groupModel.Name;

            myGroupModel = groupModel;
        }
    }
}
