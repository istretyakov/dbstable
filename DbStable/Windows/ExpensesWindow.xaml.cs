﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class ExpensesWindow : Window
    {
        private List<ExpenseModel> myExpenseModels;

        public ExpensesWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DateDatePicker.SelectedDate = DateTime.Today;

            UpdatePage();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                ExpensesListBox.Height = this.ActualHeight - 400;
            }
        }

        private void ExpensesListBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.IsLoaded)
            {
                ExpensesListBox.Height = this.ActualHeight - 400;
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            DateTime date;
            string comment = CommentTextBox.Text;
            int amount;

            try
            {
                date = (DateTime)DateDatePicker.SelectedDate;
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Дата должна быть в формате дд.мм.гггг", "Ошибка");
                return;
            }

            bool result = int.TryParse(AmountTextBox.Text, out amount);

            if (!result)
            {
                MessageBox.Show("Ошибка формата суммы", "Ошибка");
                return;
            }
            if (amount < -10000000 || amount > 10000000)
            {
                MessageBox.Show("Сумма должна быть от -10000000 до 10000000", "Ошибка");
                return;
            }
            if (comment.Length > 40)
            {
                MessageBox.Show("Длина комментария должна быть до 40 символов", "Ошибка");
                return;
            }

            ExpenseModel expenseModel = new ExpenseModel();
            expenseModel.Comment = comment;
            expenseModel.Amount = amount;            
            expenseModel.Date = date;

            PaymentModel paymentModel = new PaymentModel();
            paymentModel.Date = date;
            paymentModel.UserId = 0;
            paymentModel.Type = PaymentTypes.Other;
            paymentModel.Value = amount;

            Database.CreatePayment(paymentModel);

            paymentModel = Database.LoadLastPayment();

            expenseModel.PaymentId = paymentModel.Id;

            Database.CreateExpense(expenseModel);

            UpdatePage();

            MessageBox.Show("Новая затрата успешно создана", "Добавление");
        }

        private void PagesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PagesComboBox.SelectedIndex == -1)
                PagesComboBox.SelectedIndex = PagesComboBox.Items.Count - 1;

            UpdatePage();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            int index = ExpensesListBox.SelectedIndex;

            try
            {
                MessageBoxResult result = MessageBox.Show($"Вы уверены, что хотите удалить затрату {myExpenseModels[index].Comment}?", 
                    "Удаление", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.No)
                    return;

                Database.DeleteExpenses(myExpenseModels[index]);

                MessageBox.Show($"Затрата {myExpenseModels[index].Comment} успешна удалена", "Удаление");

                UpdatePage();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show("Выберите затрату в списке", "Ошибка");
                return;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void UpdatePage()
        {
            int pages = PagesComboBox.Items.Count;
            int count = Database.GetCountExpenses();

            if (count % 25 == 0)
                count /= 25;
            else
                count = count / 25 + 1;

            if (pages < count)
            {
                for (int i = 0; i < count - pages; i++)
                {
                    ComboBoxItem item = new ComboBoxItem();
                    item.Content = $"{PagesComboBox.Items.Count + 1}";
                    PagesComboBox.Items.Add(item);
                }
            }
            else if (pages > count)
            {
                for (int i = 0; i < pages - count; i++)
                    PagesComboBox.Items.RemoveAt(PagesComboBox.Items.Count - 1);
            }

            if (myExpenseModels != null)
                myExpenseModels.Clear();
            ExpensesListBox.Items.Clear();

            int page = PagesComboBox.SelectedIndex;

            List<ExpenseModel> expenseModels = Database.LoadExpenses(25, page * 25);
            int number = page * 25 + 1;

            foreach (ExpenseModel expenseModel in expenseModels)
            {
                ListBoxItem item = new ListBoxItem();
                item.Content = $"{number} {expenseModel.Date.ToString("dd.MM.yyyy")} " +
                    $"{expenseModel.Amount} {expenseModel.Comment}";
                ExpensesListBox.Items.Add(item);

                number++;
            }

            myExpenseModels = expenseModels;
        }
    }
}
