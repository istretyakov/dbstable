﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;
using DbStable.Classes;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class StatisticsWindow : Window
    {
        private List<StackPanel> ItemStackPanels = new List<StackPanel>();

        public StatisticsWindow()
        {
            InitializeComponent();

            this.Loaded += StatisticsWindow_Loaded;
        }

        private void StatisticsWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DateDatePicker.SelectedDate = DateTime.Today;

            DateTime dateTime = (DateTime)DateDatePicker.SelectedDate;

            SetStatsDays(dateTime, 0);
        }

        private void SetStatsDays(DateTime dateTime, int type)
        {
            DateTime startDateTime = new DateTime();
            DateTime finishDateTime = new DateTime();

            if (type == 0)
            {
                int dayOfWeek = (int)dateTime.DayOfWeek - 1;
                if (dayOfWeek == -1)
                    dayOfWeek = 6;
                startDateTime = dateTime.AddDays(-dayOfWeek);
                finishDateTime = startDateTime.AddDays(7);

                DisplayDays(startDateTime, finishDateTime);
            }
            else if (type == 1)
            {
                displayMonth(dateTime.Month, dateTime.Year);
            }
            else if (type == 2)
            {
                displayYear(dateTime.Year);
            }
        }

        private void DisplayDays(DateTime startDateTime, DateTime finishDateTime)
        {
            ItemsStackPanel.Children.Clear();

            TimeSpan t = finishDateTime - startDateTime;
            DateTime currentDateTime = startDateTime;

            for (int d = 0; d < t.Days; d++)
            {
                List<PaymentModel> paymentModels = Database.LoadPaymentsForDay(currentDateTime);

                // Границы для обрётки дня
                Border dayBorder = new Border();
                dayBorder.Margin = new Thickness(0, 0, 0, 10);
                dayBorder.BorderBrush = new SolidColorBrush(Color.FromRgb(213, 223, 229));
                dayBorder.BorderThickness = new Thickness(1);
                dayBorder.CornerRadius = new CornerRadius(2.5);
                ItemsStackPanel.Children.Add(dayBorder);

                // Обёртка для дня
                StackPanel dayStackPanel = new StackPanel();
                dayStackPanel.Margin = new Thickness(10);
                dayBorder.Child = dayStackPanel;

                // Дата в начале обёртки
                TextBlock dateTextBlock = new TextBlock();
                dateTextBlock.Text = $"{currentDateTime.ToString("dd.MM.yyyy")}";
                dateTextBlock.FontSize = 14;
                dateTextBlock.FontWeight = FontWeights.Bold;
                dayStackPanel.Children.Add(dateTextBlock);

                // Горизонтальная обёртка всей информации в дне
                StackPanel infoStackPanel = new StackPanel();
                infoStackPanel.Orientation = Orientation.Horizontal;
                dayStackPanel.Children.Add(infoStackPanel);

                TextBlock[] dataTextBlocks;
                StackPanel testStackPanel = createTables(out dataTextBlocks);
                dayStackPanel.Children.Add(testStackPanel);
                
                MoneyStats dayMoney = new MoneyStats();
                int oneTimeLessons = 0;
                int ticketLessons = 0;

                foreach (PaymentModel paymentModel in paymentModels)
                {
                    switch (paymentModel.Type)
                    {
                        case PaymentTypes.OneTime:
                            oneTimeLessons++;
                            dayMoney.OneTimePaid += paymentModel.Value;
                            break;

                        case PaymentTypes.SeasonTicket:
                            dayMoney.SeasonTicket += paymentModel.Value;
                            break;

                        case PaymentTypes.Salary:
                            dayMoney.Salary += paymentModel.Value;
                            break;

                        case PaymentTypes.Ticket:
                            ticketLessons++;
                            dayMoney.Ticket += paymentModel.Value;
                            break;

                        case PaymentTypes.NotPaid:
                            oneTimeLessons++;
                            dayMoney.NotPaid += paymentModel.Value;
                            break;

                        case PaymentTypes.Other:
                            if (paymentModel.Value > 0)
                                dayMoney.OtherPlus += paymentModel.Value;
                            else
                                dayMoney.OtherMinus += paymentModel.Value;
                            break;
                    }
                }

                int paid = dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus -
                    (dayMoney.Salary - dayMoney.OtherMinus);
                int notPaid = dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus -
                    (dayMoney.Salary - dayMoney.OtherMinus) + dayMoney.NotPaid;

                dataTextBlocks[0].Text = $"{dayMoney.OneTimePaid}";
                dataTextBlocks[1].Text = $"{dayMoney.SeasonTicket}";
                dataTextBlocks[2].Text = $"{dayMoney.OtherPlus}";
                dataTextBlocks[3].Text = $"{dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus}";
                
                dataTextBlocks[4].Text = $"{dayMoney.Salary}";
                dataTextBlocks[5].Text = $"{-dayMoney.OtherMinus}";
                dataTextBlocks[6].Text = $"{dayMoney.Salary - dayMoney.OtherMinus}";

                dataTextBlocks[7].Text = $"{paid}";
                dataTextBlocks[8].Text = $"{notPaid}";

                dataTextBlocks[9].Text = $"{dayMoney.NotPaid}";
                dataTextBlocks[10].Text = $"{dayMoney.Ticket}";

                dataTextBlocks[11].Text = $"{oneTimeLessons}";
                dataTextBlocks[12].Text = $"{ticketLessons}";

                if (paid > 0)
                    dataTextBlocks[7].Foreground = new SolidColorBrush(Color.FromRgb(11, 218, 81));
                else if (paid < 0)
                    dataTextBlocks[7].Foreground = new SolidColorBrush(Color.FromRgb(248, 0, 0));
                if (notPaid > 0)
                    dataTextBlocks[8].Foreground = new SolidColorBrush(Color.FromRgb(11, 218, 81));
                else if (notPaid < 0)
                    dataTextBlocks[8].Foreground = new SolidColorBrush(Color.FromRgb(248, 0, 0));
                if (dayMoney.NotPaid != 0)
                    dataTextBlocks[9].Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));

                currentDateTime = currentDateTime.AddDays(1);
            }
        }

        private void displayMonth(int month, int year)
        {
            ItemsStackPanel.Children.Clear();

            DateTime firstDateTime = new DateTime(year, month, 1);
            DateTime secondDateTime = new DateTime();

            int dayOfWeek = (int)firstDateTime.DayOfWeek - 1;
            if (dayOfWeek == -1)
                dayOfWeek = 6;
            firstDateTime = firstDateTime.AddDays(-dayOfWeek);
            secondDateTime = firstDateTime.AddDays(6);

            do
            {
                List<PaymentModel> paymentModels = Database.LoadPaymentsForDates(firstDateTime, secondDateTime.AddDays(1));

                // Границы для обрётки дня
                Border dayBorder = new Border();
                dayBorder.Margin = new Thickness(0, 0, 0, 10);
                dayBorder.BorderBrush = new SolidColorBrush(Color.FromRgb(213, 223, 229));
                dayBorder.BorderThickness = new Thickness(1);
                dayBorder.CornerRadius = new CornerRadius(2.5);
                ItemsStackPanel.Children.Add(dayBorder);

                // Обёртка для дня
                StackPanel dayStackPanel = new StackPanel();
                dayStackPanel.Margin = new Thickness(10);
                dayBorder.Child = dayStackPanel;

                // Дата в начале обёртки
                TextBlock dateTextBlock = new TextBlock();
                dateTextBlock.Text = $"{firstDateTime.ToString("dd.MM.yyyy")} - {secondDateTime.ToString("dd.MM.yyyy")}";
                dateTextBlock.FontSize = 14;
                dateTextBlock.FontWeight = FontWeights.Bold;
                dayStackPanel.Children.Add(dateTextBlock);

                // Горизонтальная обёртка всей информации в дне
                StackPanel infoStackPanel = new StackPanel();
                infoStackPanel.Orientation = Orientation.Horizontal;
                dayStackPanel.Children.Add(infoStackPanel);

                TextBlock[] dataTextBlocks;
                StackPanel testStackPanel = createTables(out dataTextBlocks);
                dayStackPanel.Children.Add(testStackPanel);

                MoneyStats dayMoney = new MoneyStats();
                int oneTimeLessons = 0;
                int ticketLessons = 0;

                foreach (PaymentModel paymentModel in paymentModels)
                {
                    switch (paymentModel.Type)
                    {
                        case PaymentTypes.OneTime:
                            oneTimeLessons++;
                            dayMoney.OneTimePaid += paymentModel.Value;
                            break;

                        case PaymentTypes.SeasonTicket:
                            dayMoney.SeasonTicket += paymentModel.Value;
                            break;

                        case PaymentTypes.Salary:
                            dayMoney.Salary += paymentModel.Value;
                            break;

                        case PaymentTypes.Ticket:
                            ticketLessons++;
                            dayMoney.Ticket += paymentModel.Value;
                            break;

                        case PaymentTypes.NotPaid:
                            oneTimeLessons++;
                            dayMoney.NotPaid += paymentModel.Value;
                            break;

                        case PaymentTypes.Other:
                            if (paymentModel.Value > 0)
                                dayMoney.OtherPlus += paymentModel.Value;
                            else
                                dayMoney.OtherMinus += paymentModel.Value;
                            break;
                    }
                }

                int paid = dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus -
                    (dayMoney.Salary - dayMoney.OtherMinus);
                int notPaid = dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus -
                    (dayMoney.Salary - dayMoney.OtherMinus) + dayMoney.NotPaid;

                dataTextBlocks[0].Text = $"{dayMoney.OneTimePaid}";
                dataTextBlocks[1].Text = $"{dayMoney.SeasonTicket}";
                dataTextBlocks[2].Text = $"{dayMoney.OtherPlus}";
                dataTextBlocks[3].Text = $"{dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus}";

                dataTextBlocks[4].Text = $"{dayMoney.Salary}";
                dataTextBlocks[5].Text = $"{-dayMoney.OtherMinus}";
                dataTextBlocks[6].Text = $"{dayMoney.Salary - dayMoney.OtherMinus}";

                dataTextBlocks[7].Text = $"{paid}";
                dataTextBlocks[8].Text = $"{notPaid}";

                dataTextBlocks[9].Text = $"{dayMoney.NotPaid}";
                dataTextBlocks[10].Text = $"{dayMoney.Ticket}";

                dataTextBlocks[11].Text = $"{oneTimeLessons}";
                dataTextBlocks[12].Text = $"{ticketLessons}";

                if (paid > 0)
                    dataTextBlocks[7].Foreground = new SolidColorBrush(Color.FromRgb(34, 140, 34)); // Зелёный
                else if (paid < 0)
                    dataTextBlocks[7].Foreground = new SolidColorBrush(Color.FromRgb(248, 0, 0));   // Красный
                if (notPaid > 0)
                    dataTextBlocks[8].Foreground = new SolidColorBrush(Color.FromRgb(34, 140, 34));
                else if (notPaid < 0)
                    dataTextBlocks[8].Foreground = new SolidColorBrush(Color.FromRgb(248, 0, 0));
                if (dayMoney.NotPaid != 0)
                    dataTextBlocks[9].Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));

                firstDateTime = firstDateTime.AddDays(7);
                secondDateTime = secondDateTime.AddDays(7);

            } while (firstDateTime.Month == month);
        }

        private void displayYear(int year)
        {
            ItemsStackPanel.Children.Clear();

            DateTime firstDateTime = new DateTime(year, 1, 1);
            DateTime secondDateTime = new DateTime();

            int dayOfWeek = (int)firstDateTime.DayOfWeek - 1;
            if (dayOfWeek == -1)
                dayOfWeek = 6;
            firstDateTime = firstDateTime.AddDays(-dayOfWeek);

            secondDateTime = firstDateTime.AddMonths(1);
            dayOfWeek = (int)secondDateTime.DayOfWeek - 1;
            if (dayOfWeek == -1)
                dayOfWeek = 6 - dayOfWeek;
            secondDateTime = secondDateTime.AddDays(dayOfWeek);

            do
            {
                List<PaymentModel> paymentModels = Database.LoadPaymentsForDates(firstDateTime, secondDateTime.AddDays(1));

                TimeSpan days = secondDateTime.AddDays(1) - firstDateTime;
                int weeks = days.Days / 7;
                if (days.Days % 7 != 0)
                    weeks++;

                // Границы для обрётки дня
                Border dayBorder = new Border();
                dayBorder.Margin = new Thickness(0, 0, 0, 10);
                dayBorder.BorderBrush = new SolidColorBrush(Color.FromRgb(213, 223, 229));
                dayBorder.BorderThickness = new Thickness(1);
                dayBorder.CornerRadius = new CornerRadius(2.5);
                ItemsStackPanel.Children.Add(dayBorder);

                // Обёртка для дня
                StackPanel dayStackPanel = new StackPanel();
                dayStackPanel.Margin = new Thickness(10);
                dayBorder.Child = dayStackPanel;

                // Дата в начале обёртки
                TextBlock dateTextBlock = new TextBlock();
                dateTextBlock.Text = $"{firstDateTime.ToString("dd.MM.yyyy")} - " +
                    $"{secondDateTime.ToString("dd.MM.yyyy")} ({weeks} недел{((weeks == 4) ? 'и' : 'ь')})";
                dateTextBlock.FontSize = 14;
                dateTextBlock.FontWeight = FontWeights.Bold;
                dayStackPanel.Children.Add(dateTextBlock);

                // Горизонтальная обёртка всей информации в дне
                StackPanel infoStackPanel = new StackPanel();
                infoStackPanel.Orientation = Orientation.Horizontal;
                dayStackPanel.Children.Add(infoStackPanel);

                TextBlock[] dataTextBlocks;
                StackPanel testStackPanel = createTables(out dataTextBlocks);
                dayStackPanel.Children.Add(testStackPanel);

                MoneyStats dayMoney = new MoneyStats();
                int oneTimeLessons = 0;
                int ticketLessons = 0;

                foreach (PaymentModel paymentModel in paymentModels)
                {
                    switch (paymentModel.Type)
                    {
                        case PaymentTypes.OneTime:
                            oneTimeLessons++;
                            dayMoney.OneTimePaid += paymentModel.Value;
                            break;

                        case PaymentTypes.SeasonTicket:
                            dayMoney.SeasonTicket += paymentModel.Value;
                            break;

                        case PaymentTypes.Salary:
                            dayMoney.Salary += paymentModel.Value;
                            break;

                        case PaymentTypes.Ticket:
                            ticketLessons++;
                            dayMoney.Ticket += paymentModel.Value;
                            break;

                        case PaymentTypes.NotPaid:
                            oneTimeLessons++;
                            dayMoney.NotPaid += paymentModel.Value;
                            break;

                        case PaymentTypes.Other:
                            if (paymentModel.Value > 0)
                                dayMoney.OtherPlus += paymentModel.Value;
                            else
                                dayMoney.OtherMinus += paymentModel.Value;
                            break;
                    }
                }

                int paid = dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus - 
                    (dayMoney.Salary - dayMoney.OtherMinus);
                int notPaid = dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus - 
                    (dayMoney.Salary - dayMoney.OtherMinus) + dayMoney.NotPaid;

                dataTextBlocks[0].Text = $"{dayMoney.OneTimePaid}";
                dataTextBlocks[1].Text = $"{dayMoney.SeasonTicket}";
                dataTextBlocks[2].Text = $"{dayMoney.OtherPlus}";
                dataTextBlocks[3].Text = $"{dayMoney.OneTimePaid + dayMoney.SeasonTicket + dayMoney.OtherPlus}";

                dataTextBlocks[4].Text = $"{dayMoney.Salary}";
                dataTextBlocks[5].Text = $"{-dayMoney.OtherMinus}";
                dataTextBlocks[6].Text = $"{dayMoney.Salary - dayMoney.OtherMinus}";

                dataTextBlocks[7].Text = $"{paid}";
                dataTextBlocks[8].Text = $"{notPaid}";

                dataTextBlocks[9].Text = $"{dayMoney.NotPaid}";
                dataTextBlocks[10].Text = $"{dayMoney.Ticket}";

                dataTextBlocks[11].Text = $"{oneTimeLessons}";
                dataTextBlocks[12].Text = $"{ticketLessons}";
                
                if (paid > 0)
                    dataTextBlocks[7].Foreground = new SolidColorBrush(Color.FromRgb(11, 218, 81));
                else if (paid < 0)
                    dataTextBlocks[7].Foreground = new SolidColorBrush(Color.FromRgb(248, 0, 0));
                if (notPaid > 0)
                    dataTextBlocks[8].Foreground = new SolidColorBrush(Color.FromRgb(11, 218, 81));
                else if (notPaid < 0)
                    dataTextBlocks[8].Foreground = new SolidColorBrush(Color.FromRgb(248, 0, 0));
                if (dayMoney.NotPaid != 0)
                    dataTextBlocks[9].Foreground = new SolidColorBrush(Color.FromRgb(0, 0, 255));

                firstDateTime = secondDateTime.AddDays(1);
                secondDateTime = secondDateTime.AddMonths(1);
                dayOfWeek = (int)secondDateTime.DayOfWeek - 1;
                if (dayOfWeek == -1)
                    dayOfWeek = 6 - dayOfWeek;
                secondDateTime = secondDateTime.AddDays(dayOfWeek);

            } while (firstDateTime.Year == year);
        }

        private void DateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                int type = TypeComboBox.SelectedIndex;
                if (type >= 0 && type <= 2)
                {
                    DateTime dateTime = (DateTime)DateDatePicker.SelectedDate;
                    SetStatsDays(dateTime, type);
                }
            }
        }

        private void TypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                int type = TypeComboBox.SelectedIndex;
                if (type >= 0 && type <= 2)
                {
                    DateTime dateTime = (DateTime)DateDatePicker.SelectedDate;
                    SetStatsDays(dateTime, type);
                }
            }
        }

        private StackPanel createTables(out TextBlock[] dataTextBlocks)
        {
            StackPanel tablesStackPanel = new StackPanel();     // Обёртка всего создаваемого
            StackPanel[] columnStackPanels = new StackPanel[2]; // Колонки разделов

            Grid[] tableGrids = new Grid[5];                    // Таблицы

            TextBlock[] blockTextBlocks = new TextBlock[13];    // Статический текст в таблицах
            TextBlock[] describeTextBlocks = new TextBlock[5];  // Описание таблиц
            dataTextBlocks = new TextBlock[13];                 // Данные в таблицах

            tablesStackPanel.Orientation = Orientation.Horizontal;

            for (int i = 0; i < 2; i++)
            {
                columnStackPanels[i] = new StackPanel();
                tablesStackPanel.Children.Add(columnStackPanels[i]);
            }

            for (int i = 0; i < 13; i++)
            {
                blockTextBlocks[i] = new TextBlock();
                dataTextBlocks[i] = new TextBlock();
            }

            for (int i = 0; i < 5; i++)
            {
                describeTextBlocks[i] = new TextBlock();
                describeTextBlocks[i].FontSize = 13;
                describeTextBlocks[i].FontWeight = FontWeights.Bold;
                describeTextBlocks[i].TextAlignment = TextAlignment.Center;
            }

            columnStackPanels[0].Margin = new Thickness(0, 0, 10, 0);

            // Таблица доходов
            describeTextBlocks[0].Text = "Доходы";
            describeTextBlocks[0].Margin = new Thickness(0, 0, 0, 10);
            columnStackPanels[0].Children.Add(describeTextBlocks[0]);

            tableGrids[0] = createTable(new string[] { "Разовые (оплаченные)", "Покупка абонемента", "Другое", "Всего" }, blockTextBlocks, dataTextBlocks, 0, 4);
            columnStackPanels[0].Children.Add(tableGrids[0]);

            // Таблица расходов
            describeTextBlocks[1].Text = "Расходы";
            describeTextBlocks[1].Margin = new Thickness(0, 10, 0, 10);
            columnStackPanels[0].Children.Add(describeTextBlocks[1]);

            tableGrids[1] = createTable(new string[] { "Зарплата", "Другое", "Всего" }, blockTextBlocks, dataTextBlocks, 4, 3);
            columnStackPanels[0].Children.Add(tableGrids[1]);

            // Таблица с итогом
            describeTextBlocks[2].Text = "Результат";
            describeTextBlocks[2].Margin = new Thickness(0, 0, 0, 10);
            columnStackPanels[1].Children.Add(describeTextBlocks[2]);

            tableGrids[2] = createTable(new string[] { "Итог", "Итог (с неоплаченными)" }, blockTextBlocks, dataTextBlocks, 7, 2);
            columnStackPanels[1].Children.Add(tableGrids[2]);

            // Таблица с нейтральными оплатами
            describeTextBlocks[3].Text = "Нейтральные";
            describeTextBlocks[3].Margin = new Thickness(0, 10, 0, 10);
            columnStackPanels[1].Children.Add(describeTextBlocks[3]);

            tableGrids[3] = createTable(new string[] { "Не оплачено", "Реализованные абонементы" }, blockTextBlocks, dataTextBlocks, 9, 2);
            columnStackPanels[1].Children.Add(tableGrids[3]);

            // Таблица с количеством
            describeTextBlocks[4].Text = "Количество занятий";
            describeTextBlocks[4].Margin = new Thickness(0, 10, 0, 10);
            columnStackPanels[1].Children.Add(describeTextBlocks[4]);

            tableGrids[4] = createTable(new string[] { "Разовые", "Абонемент" }, blockTextBlocks, dataTextBlocks, 11, 2);
            columnStackPanels[1].Children.Add(tableGrids[4]);

            return tablesStackPanel;
        }

        private Grid createTable(string[] texts, TextBlock[] blockTextBlocks, TextBlock[] dataTextBlocks, int nStart, int n)
        {
            Grid tableGrid = new Grid();

            Border[] cellBorder = new Border[n * 2];

            ColumnDefinition[] column = new ColumnDefinition[2];
            for (int i = 0; i < 2; i++)
            {
                column[i] = new ColumnDefinition();
                tableGrid.ColumnDefinitions.Add(column[i]);
            }

            column[0].Width = new GridLength(180);
            column[1].Width = new GridLength(80);

            for (int i = 0; i < n; i++)
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(30);
                tableGrid.RowDefinitions.Add(row);
            }

            for (int i = nStart; i < nStart + n; i++)
            {
                blockTextBlocks[i].Text = texts[i - nStart];
                blockTextBlocks[i].Margin = new Thickness(5, 0, 0, 0);
                blockTextBlocks[i].VerticalAlignment = VerticalAlignment.Center;
                blockTextBlocks[i].FontSize = 13;

                dataTextBlocks[i].Margin = new Thickness(5, 0, 0, 0);
                dataTextBlocks[i].VerticalAlignment = VerticalAlignment.Center;
                dataTextBlocks[i].FontSize = 13;
            }

            for (int i = nStart; i < nStart + n; i++)
            {
                cellBorder[i - nStart] = new Border();
                cellBorder[i - nStart].BorderBrush = new SolidColorBrush(Colors.Black);
                cellBorder[i - nStart].BorderThickness = new Thickness(1, 1, 0, 0);
                tableGrid.Children.Add(cellBorder[i - nStart]);
                Grid.SetColumn(cellBorder[i - nStart], 0);
                Grid.SetRow(cellBorder[i - nStart], i - nStart);

                cellBorder[i - nStart + n] = new Border();
                cellBorder[i - nStart + n].BorderBrush = new SolidColorBrush(Colors.Black);
                cellBorder[i - nStart + n].BorderThickness = new Thickness(1, 1, 1, 0);
                tableGrid.Children.Add(cellBorder[i - nStart + n]);
                Grid.SetColumn(cellBorder[i - nStart + n], 1);
                Grid.SetRow(cellBorder[i - nStart + n], i - nStart);

                cellBorder[i - nStart].Child = blockTextBlocks[i];
                cellBorder[i - nStart + n].Child = dataTextBlocks[i];
            }

            cellBorder[n - 1].BorderBrush = new SolidColorBrush(Colors.Black);
            cellBorder[n - 1].BorderThickness = new Thickness(1, 1, 0, 1);
            cellBorder[n * 2 - 1].BorderBrush = new SolidColorBrush(Colors.Black);
            cellBorder[n * 2 - 1].BorderThickness = new Thickness(1, 1, 1, 1);

            return tableGrid;
        }
    }
}
