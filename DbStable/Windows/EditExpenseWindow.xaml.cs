﻿using System;
using System.Windows;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class EditExpenseWindow : Window
    {
        public ExpenseModel MyExpenseModel { get; set; }

        public EditExpenseWindow()
        {
            InitializeComponent();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            DateTime date;
            string comment = CommentTextBox.Text;
            int amount;

            try
            {
                date = (DateTime)DateDatePicker.SelectedDate;
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Дата должна быть в формате дд.мм.гггг", "Ошибка");
                return;
            }

            bool result = int.TryParse(AmountTextBox.Text, out amount);

            if (!result)
            {
                MessageBox.Show("Ошибка формата суммы", "Ошибка");
                return;
            }
            if (amount < -10000000 || amount > 10000000)
            {
                MessageBox.Show("Сумма должна быть от -10000000 до 10000000", "Ошибка");
                return;
            }
            if (comment.Length > 40)
            {
                MessageBox.Show("Длина комментария должна быть до 40 символов", "Ошибка");
                return;
            }

            if (MyExpenseModel == null)
            {
                MessageBox.Show("Модель не существует", "Ошибка");
                this.Close();
                return;
            }

            MyExpenseModel.Comment = comment;
            MyExpenseModel.Amount = amount;
            MyExpenseModel.Date = date;

            Database.SaveExpense(MyExpenseModel);

            PaymentModel paymentModel = Database.LoadPayment(MyExpenseModel.Id);
            paymentModel.Value = amount;
            Database.SavePayment(paymentModel);

            ExpensesWindow expensesWindow = this.Parent as ExpensesWindow;
            if (expensesWindow != null)
                expensesWindow.UpdatePage();

            MessageBox.Show($"Затрата {MyExpenseModel.Comment} успешно отредактирована", "Редактирование");
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
