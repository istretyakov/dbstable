﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class EditUserWindow : Window
    {
        private UserModel myUserModel;
        private Button[] myLearningsButtons = new Button[5];
        private LessonModel[] myLessonModels = new LessonModel[5];

        public EditUserWindow()
        {
            InitializeComponent();

            this.Loaded += Window_Loaded;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            List<GroupModel> groupModels = Database.LoadAllGroups();

            foreach (GroupModel groupModel in groupModels)      // Добавление групп персон в ComboBox
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = groupModel.Name;
                GroupsComboBox.Items.Add(item);
            }

            ShowLessons();
        }

        public void ShowLessons()
        {
            List<LessonModel> lessonModels = Database.LoadLessons(myUserModel.Id, DateTime.Today, 5);

            if (lessonModels.Count != 0)
            {
                LearningsStackPanel.Children.Clear();

                for (int i = 0; i < lessonModels.Count; i++)
                {
                    Button btn = new Button();
                    if (i != 0)
                        btn.Margin = new Thickness(0, 5, 0, 0);
                    btn.Width = 120;
                    btn.Height = 24;
                    btn.Content = $"{lessonModels[i].StartTime.ToString("dd.MM.yyyy HH:mm")}";
                    btn.Click += LearningButton_Click;
                    myLearningsButtons[i] = btn;
                    myLessonModels[i] = lessonModels[i];
                    LearningsStackPanel.Children.Add(btn);
                }
            }
        }

        private void LearningButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                for (int i = 0; i < myLearningsButtons.Length; i++)
                {
                    if (myLearningsButtons[i] == btn)
                    {
                        LessonWindow lessonWindow = new LessonWindow();
                        lessonWindow.CurrentLesson = myLessonModels[i];
                        lessonWindow.SetType(1);
                        lessonWindow.Owner = this;
                        lessonWindow.ShowDialog();
                        break;
                    }
                }
            }
        }

        private void SaveUserButton_Click(object sender, RoutedEventArgs e)
        {
            string name = NameTextBox.Text;
            int group = GroupsComboBox.SelectedIndex + 1;
            int age;
            bool r = int.TryParse(AgeTextBox.Text, out age);
            if (!r)
            {
                MessageBox.Show($"Вы указали неправильный формат возраста");
                return;
            }
            if (age <= 0 || age > 100)
            {
                MessageBox.Show($"Вы указали недопускаемый возраст");
                return;
            }
            string number = NumberTextBox.Text;

            myUserModel.Name = name;
            myUserModel.Group = group;
            myUserModel.Age = age;
            myUserModel.Number = number;
            Database.SaveUser(myUserModel);

            MessageBox.Show($"Вы сохранили персону {myUserModel.Name}");

            UsersWindow usersWindow = this.Owner as UsersWindow;

            this.Close();

            if (usersWindow != null)
                usersWindow.UpdatePage();       // Обновить список с персонами в окне,
                                                // откуда было открыто это окно
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        // Установка начальных данных при открытии страницы
        public void SetInfo(string name)
        {
            UserModel userModel = Database.LoadUser(name);

            NameTextBox.Text = userModel.Name;
            GroupsComboBox.SelectedIndex = userModel.Group - 1;
            AgeTextBox.Text = $"{userModel.Age}";
            NumberTextBox.Text = $"{userModel.Number}";

            myUserModel = userModel;
        }
    }
}