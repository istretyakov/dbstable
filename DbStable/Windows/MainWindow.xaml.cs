﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace DbStable.Windows
{
    public partial class MainWindow : Window
    {
        private List<StackPanel> weekStackPanels = new List<StackPanel>();
        private List<Button> dayButtons = new List<Button>();
        private List<DateTime> dayDateTimes = new List<DateTime>();

        private DateTime currentDate;
        private DateTime firstDate;
        private DateTime lastDate;

        public MainWindow()
        {
            InitializeComponent();

            currentDate = DateTime.Now;

            this.Loaded += MainWindow_Loaded;
            this.SizeChanged += MainWindow_SizeChanged;
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                foreach (Button dayButton in dayButtons)
                {
                    dayButton.Width = CalendarScroll.ActualWidth / 7;
                    dayButton.Height = CalendarScroll.ActualHeight / 5;
                }
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var uri = new Uri("MainTheme.xaml", UriKind.Relative);
            // загружаем словарь ресурсов
            ResourceDictionary resourceDict = Application.LoadComponent(uri) as ResourceDictionary;
            // очищаем коллекцию ресурсов приложения
            Application.Current.Resources.Clear();
            // добавляем загруженный словарь ресурсов
            Application.Current.Resources.MergedDictionaries.Add(resourceDict);

            DateTime date = new DateTime(currentDate.Year, currentDate.Month, 1);
            date = date.AddDays(-35 - (int)date.DayOfWeek + 1);
            firstDate = date;

            DateNameTextBlock.Text = $"{GetMonthString(currentDate.Month)} {currentDate.Year}";

            for (int i = 0; i < 15; i++)
            {
                StackPanel weekStackPanel = new StackPanel();
                weekStackPanel.Orientation = Orientation.Horizontal;
                weekStackPanels.Add(weekStackPanel);
                CalendarStack.Children.Add(weekStackPanel);

                for (int j = 0; j < 7; j++)
                {
                    Button dayButton = new Button();
                    dayButton.Width = CalendarScroll.ActualWidth / 7;
                    dayButton.Height = CalendarScroll.ActualHeight / 5;
                    if (date.Month % 2 == 0)
                        dayButton.Style = (Style)FindResource("DayWhiteButtonStyle");
                    else
                        dayButton.Style = (Style)FindResource("DayGreyButtonStyle");
                    dayButton.HorizontalContentAlignment = HorizontalAlignment.Left;
                    dayButton.VerticalContentAlignment = VerticalAlignment.Top;
                    if (date.Day == 1)
                        dayButton.Content = $"{date.Day}.{date.Month}";
                    else
                        dayButton.Content = $"{date.Day}";
                    dayButton.Click += DayButton_Click;
                    dayButtons.Add(dayButton);
                    weekStackPanel.Children.Add(dayButton);
                    DateTime dateTime = new DateTime();
                    dateTime = date;
                    dayDateTimes.Add(dateTime);
                    date = date.AddDays(1);
                }
            }

            lastDate = date;
            
            CalendarScroll.ScrollToVerticalOffset(CalendarScroll.ActualHeight);
        }

        private void LessonsButton_Click(object sender, RoutedEventArgs e)
        {
            LessonWindow lessonWindow = new LessonWindow();
            lessonWindow.WindowState = this.WindowState;
            lessonWindow.ChosenDate = DateTime.Now;
            lessonWindow.SetType(0);
            lessonWindow.ShowDialog();
        }

        private void PersonsButton_Click(object sender, RoutedEventArgs e)
        {
            UsersWindow usersWindow = new UsersWindow();
            usersWindow.WindowState = this.WindowState;
            usersWindow.ShowDialog();
        }

        private void HorsesButton_Click(object sender, RoutedEventArgs e)
        {
            HorsesWindow horsesWindow = new HorsesWindow();
            horsesWindow.WindowState = this.WindowState;
            horsesWindow.ShowDialog();
        }

        private void GroupsButton_Click(object sender, RoutedEventArgs e)
        {
            GroupsWindow groupsWindow = new GroupsWindow();
            groupsWindow.WindowState = this.WindowState;
            groupsWindow.ShowDialog();
        }

        private void CalendarScroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                DateNameTextBlock.Text = $"{GetMonthString(dayDateTimes[(int)(e.VerticalOffset / (CalendarScroll.ActualHeight / 5)) * 7 + 6].Month)} " +
                    $"{dayDateTimes[(int)(e.VerticalOffset / (CalendarScroll.ActualHeight / 5)) * 7 + 6].Year}";

                if (e.ExtentHeightChange == 0.0)
                {
                    double offset = CalendarScroll.ContentVerticalOffset;

                    if (offset >= CalendarScroll.ScrollableHeight - 1)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            if (weekStackPanels[0] != null)
                            {
                                StackPanel stackPanel = weekStackPanels[0];
                                weekStackPanels.Remove(stackPanel);
                                weekStackPanels.Add(stackPanel);
                                CalendarStack.Children.Remove(stackPanel);
                                CalendarStack.Children.Add(stackPanel);

                                for (int j = 0; j < 7; j++)
                                {
                                    Button dayButton = dayButtons[0];
                                    dayButtons.Remove(dayButton);
                                    dayButtons.Add(dayButton);

                                    DateTime dayDateTime = dayDateTimes[0];
                                    dayDateTimes.Remove(dayDateTime);
                                    dayDateTime = lastDate;
                                    dayDateTimes.Add(dayDateTime);

                                    if (lastDate.Day == 1)
                                        dayButton.Content = $"{lastDate.Day}.{lastDate.Month}";
                                    else
                                        dayButton.Content = $"{lastDate.Day}";

                                    lastDate = lastDate.AddDays(1);
                                    firstDate = firstDate.AddDays(1);
                                }
                            }
                        }

                        currentDate = firstDate.AddDays(35);

                        CalendarScroll.ScrollToVerticalOffset(CalendarScroll.ActualHeight);
                    }
                    else if (offset < 1)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            if (weekStackPanels[weekStackPanels.Count - 1] != null)
                            {
                                StackPanel stackPanel = weekStackPanels[weekStackPanels.Count - 1];
                                weekStackPanels.Remove(stackPanel);
                                weekStackPanels.Insert(0, stackPanel);
                                CalendarStack.Children.Remove(stackPanel);
                                CalendarStack.Children.Insert(0, stackPanel);

                                for (int j = 0; j < 7; j++)
                                {
                                    lastDate = lastDate.AddDays(-1);
                                    firstDate = firstDate.AddDays(-1);

                                    Button dayButton = dayButtons[dayButtons.Count - 1];
                                    dayButtons.Remove(dayButton);
                                    dayButtons.Insert(0, dayButton);

                                    DateTime dayDateTime = dayDateTimes[dayDateTimes.Count - 1];
                                    dayDateTimes.Remove(dayDateTime);
                                    dayDateTime = firstDate;
                                    dayDateTimes.Insert(0, dayDateTime);

                                    if (firstDate.Day == 1)
                                        dayButton.Content = $"{firstDate.Day}.{firstDate.Month}";
                                    else
                                        dayButton.Content = $"{firstDate.Day}";
                                }
                            }
                        }

                        currentDate = firstDate.AddDays(35);

                        CalendarScroll.ScrollToVerticalOffset(CalendarScroll.ActualHeight);
                    }
                }
            }
        }

        private void DayButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;

            if (btn != null)
            {
                for (int i = 0; i < 15 * 7; i++)
                {
                    if (dayButtons[i] == btn)
                    {
                        DayWindow dayWindow = new DayWindow();
                        dayWindow.WindowState = this.WindowState;
                        dayWindow.ChoosedDay = dayDateTimes[i];
                        dayWindow.ShowDialog();
                        break;
                    }
                }
            }
        }

        private void StatisticsButton_Click(object sender, RoutedEventArgs e)
        {
            StatisticsWindow statiscticsWindow = new StatisticsWindow();
            statiscticsWindow.WindowState = this.WindowState;
            statiscticsWindow.ShowDialog();
        }

        private string GetMonthString(int month)
        {
            string name = null;

            switch (month)
            {
                case 1:
                    name = "Январь";
                    break;
                case 2:
                    name = "Февраль";
                    break;
                case 3:
                    name = "Март";
                    break;
                case 4:
                    name = "Апрель";
                    break;
                case 5:
                    name = "Май";
                    break;
                case 6:
                    name = "Июнь";
                    break;
                case 7:
                    name = "Июль";
                    break;
                case 8:
                    name = "Август";
                    break;
                case 9:
                    name = "Сентябрь";
                    break;
                case 10:
                    name = "Октябрь";
                    break;
                case 11:
                    name = "Ноябрь";
                    break;
                case 12:
                    name = "Декабрь";
                    break;
            }

            return name;
        }

        private void ExpensesButton_Click(object sender, RoutedEventArgs e)
        {
            ExpensesWindow expensesWindow = new ExpensesWindow();
            expensesWindow.WindowState = this.WindowState;
            expensesWindow.ShowDialog();
        }

        private void CalendarScroll_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (e.Delta > 0)
                CalendarScroll.ScrollToVerticalOffset(CalendarScroll.VerticalOffset - CalendarScroll.ActualHeight / 5);
            else if (e.Delta < 0)
                CalendarScroll.ScrollToVerticalOffset(CalendarScroll.VerticalOffset + CalendarScroll.ActualHeight / 5);
        }
    }
}