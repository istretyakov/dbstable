﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class UsersWindow : Window
    {
        private List<UserModel> myUserModels;

        public UsersWindow()
        {
            InitializeComponent();

            this.Loaded += UserWindow_Loaded;
        }

        private void UserWindow_Loaded(object sender, RoutedEventArgs e)
        {
            List<GroupModel> groupModels = Database.LoadAllGroups();
            foreach (GroupModel groupModel in groupModels)
            {
                ComboBoxItem itemNew = new ComboBoxItem();
                itemNew.Content = groupModel.Name;

                NewGroupComboBox.Items.Add(itemNew);

                ComboBoxItem itemFound = new ComboBoxItem();
                itemFound.Content = groupModel.Name;

                FoundGroupComboBox.Items.Add(itemFound);
            }
        }

        private void AddUserButton_Click(object sender, RoutedEventArgs e)
        {
            string name = NewNameTextBox.Text;

            if (name.Length <= 0)
            {
                MessageBox.Show("Поле имени пусто");
                return;
            }
            else if (name.Length > 40)
            {
                MessageBox.Show("Имя должно состоять не более, чем из 40 символов");
                return;
            }
            else if (Database.DoesUserNameExist(name))
            {
                MessageBox.Show("Персона с таким именем уже существует");
                return;
            }

            UserModel userModel = new UserModel();
            userModel.Name = name;
            userModel.Group = NewGroupComboBox.SelectedIndex + 1;
            userModel.Age = 18;
            userModel.Number = "88001111111";
            Database.CreateUser(userModel);
            userModel.Id = Database.GetUserIdByName(userModel.Name);

            UpdatePage();

            MessageBox.Show($"Персона {userModel.Name} успешно добавлена");
        }

        private void FindUserButton_Click(object sender, RoutedEventArgs e)
        {
            UpdatePage();
        }

        private void EditUserButton_Click(object sender, RoutedEventArgs e)
        {
            int index = UsersListBox.SelectedIndex;

            try
            {
                UserModel userModel = myUserModels[index];

                EditUserWindow editUserWindow = new EditUserWindow();
                editUserWindow.Owner = this;
                editUserWindow.SetInfo(userModel.Name);
                editUserWindow.ShowDialog();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show($"Неправильный индекс в списке: {index}");
            }
        }

        private void DeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            int index = UsersListBox.SelectedIndex;

            try
            {
                UserModel userModel = myUserModels[index];

                MessageBoxResult result = MessageBox.Show($"Вы уверены, что хотите удалить персону {userModel.Name}?", 
                    "Удаление персоны", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.Yes)
                {
                    Database.DeleteUser(userModel.Id);
                    MessageBox.Show($"Вы удалили персону {userModel.Name}");
                    UpdatePage();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show($"Неправильный индекс в списке: {index}");
            }
        }

        private void LoadUsersInfo(int page, int amount, int group, string nameMatch)
        {
            int index = (page - 1) * amount + 1;

            UsersListBox.Items.Clear();
            if (myUserModels != null)
                myUserModels.Clear();

            List<UserModel> userModels = Database.LoadUsers(25, index - 1, group, nameMatch);

            foreach (UserModel userModel in userModels)
            {
                ListBoxItem item = new ListBoxItem();
                item.Content = $"{index} {userModel.Name}";
                UsersListBox.Items.Add(item);

                index++;
            }

            myUserModels = userModels;
        }

        private void PagesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PagesComboBox.SelectedIndex == -1)
                PagesComboBox.SelectedIndex = PagesComboBox.Items.Count - 1;

            UpdatePage();
        }

        public void UpdatePage()
        {
            int group = FoundGroupComboBox.SelectedIndex;
            string nameMatch = FoundNameTextBox.Text;
            ComboBoxItem item;
            int rows = Database.GetCountUsers(group, nameMatch);
            int pages = (rows - 1) / 25 + 1;
            int count = PagesComboBox.Items.Count;

            if (pages < 0)
                pages = 0;

            if (pages > count)
            {
                for (int i = 0; i < pages - count; i++)
                {
                    item = new ComboBoxItem();
                    item.Content = PagesComboBox.Items.Count + 1;
                    PagesComboBox.Items.Add(item);
                }
            }
            else if (pages < count)
            {
                while (pages != PagesComboBox.Items.Count && PagesComboBox.Items.Count > 0)
                    PagesComboBox.Items.RemoveAt(PagesComboBox.Items.Count - 1);
            }

            int page;
            item = PagesComboBox.SelectedItem as ComboBoxItem;
            if (item != null)
            {
                int.TryParse(Convert.ToString(item.Content), out page);
                LoadUsersInfo(page, 25, group, nameMatch);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            FoundNameTextBox.Text = "";
            FoundGroupComboBox.SelectedIndex = 0;

            UpdatePage();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void SetTypeUsersWindow(int type)
        {
            if (type == 0)
            {
                ChooseButton.Margin = new Thickness(0, 0, 0, 0);
                ChooseButton.Width = 0;
                ChooseButton.Visibility = Visibility.Hidden;
            }
            else if (type == 1)
            {
                ChooseButton.Margin = new Thickness(0, 0, 10, 0);
                ChooseButton.Width = 70;
                ChooseButton.Visibility = Visibility.Visible;

                EditButton.Margin = new Thickness(0, 0, 0, 0);
                EditButton.Width = 0;
                EditButton.Visibility = Visibility.Hidden;

                DeleteButton.Margin = new Thickness(0, 0, 0, 0);
                DeleteButton.Width = 0;
                DeleteButton.Visibility = Visibility.Hidden;
            }
        }

        private void ChooseButton_Click(object sender, RoutedEventArgs e)
        {
            int index = UsersListBox.SelectedIndex;

            if (index == -1)
            {
                MessageBox.Show("Вы не выбрали персону");
                return;
            }

            try
            {
                UserModel userModel = myUserModels[index];

                LessonWindow lessonWindow = this.Owner as LessonWindow;
                if (lessonWindow != null)
                    lessonWindow.GiveUserModel(userModel);

                this.Close();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show($"Неправильный индекс в списке: {index}");
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                UsersListBox.Height = this.ActualHeight - 400;
            }
        }

        private void UsersListBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.IsLoaded)
            {
                UsersListBox.Height = this.ActualHeight - 400;
            }
        }
    }
}
