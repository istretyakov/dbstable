﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class HorsesWindow : Window
    {
        private List<HorseModel> myHorseModels;

        public HorsesWindow()
        {
            InitializeComponent();
        }

        private void AddHorseButton_Click(object sender, RoutedEventArgs e)
        {
            string name = NewNameTextBox.Text;

            if (name.Length <= 0)
            {
                MessageBox.Show("Поле имени пусто");
                return;
            }
            else if (name.Length > 40)
            {
                MessageBox.Show("Имя должно состоять не более, чем из 40 символов");
                return;
            }
            else if (Database.DoesHorseNameExist(name))
            {
                MessageBox.Show("Конь с таким именем уже существует");
                return;
            }

            HorseModel horseModel = new HorseModel();
            horseModel.Name = name;
            Database.CreateHorse(horseModel);
            horseModel.Id = Database.GetHorseIdByName(horseModel.Name);

            UpdatePage();

            MessageBox.Show($"Конь {horseModel.Name} успешно добавлен");
        }

        private void FindHorseButton_Click(object sender, RoutedEventArgs e)
        {
            UpdatePage();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            FoundNameTextBox.Text = "";

            UpdatePage();
        }

        private void EditHorseButton_Click(object sender, RoutedEventArgs e)
        {
            int index = HorsesListBox.SelectedIndex;

            try
            {
                HorseModel horseModel = myHorseModels[index];

                EditHorseWindow editHorseWindow = new EditHorseWindow();
                editHorseWindow.Owner = this;
                editHorseWindow.SetInfo(horseModel.Name);
                editHorseWindow.ShowDialog();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show($"Неправильный индекс в списке: {index}");
            }
        }

        private void DeleteHorseButton_Click(object sender, RoutedEventArgs e)
        {
            int index = HorsesListBox.SelectedIndex;

            try
            {
                HorseModel horseModel = myHorseModels[index];

                MessageBoxResult result = MessageBox.Show($"Вы уверены, что хотите удалить коня {horseModel.Name}?",
                    "Удаление коня", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.Yes)
                {
                    Database.DeleteHorse(horseModel.Id);
                    MessageBox.Show($"Вы удалили коня {horseModel.Name}");
                    UpdatePage();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show($"Неправильный индекс в списке: {index}");
            }
        }

        private void LoadHorseInfo(int page, int amount, string nameMatch = "")
        {
            int index = (page - 1) * amount + 1;

            HorsesListBox.Items.Clear();
            if (myHorseModels != null)
                myHorseModels.Clear();

            List<HorseModel> horseModels = Database.LoadHorses(25, index - 1, nameMatch);

            foreach (HorseModel horseModel in horseModels)
            {
                ListBoxItem item = new ListBoxItem();
                item.Content = $"{index} {horseModel.Name}";
                HorsesListBox.Items.Add(item);

                index++;
            }

            myHorseModels = horseModels;
        }

        private void PagesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PagesComboBox.SelectedIndex == -1)
                PagesComboBox.SelectedIndex = PagesComboBox.Items.Count - 1;

            UpdatePage();
        }

        public void UpdatePage()
        {
            string nameMatch = FoundNameTextBox.Text;
            ComboBoxItem item;
            int rows = Database.GetCountHorses(nameMatch);
            int pages = (rows - 1) / 25 + 1;
            int count = PagesComboBox.Items.Count;

            if (pages < 0)
                pages = 0;

            if (pages > count)
            {
                for (int i = 0; i < pages - count; i++)
                {
                    item = new ComboBoxItem();
                    item.Content = PagesComboBox.Items.Count + 1;
                    PagesComboBox.Items.Add(item);
                }
            }
            else if (pages < count)
            {
                while (pages != PagesComboBox.Items.Count && PagesComboBox.Items.Count > 0)
                    PagesComboBox.Items.RemoveAt(PagesComboBox.Items.Count - 1);
            }

            int page;
            item = PagesComboBox.SelectedItem as ComboBoxItem;
            if (item != null)
            {
                int.TryParse(Convert.ToString(item.Content), out page);
                LoadHorseInfo(page, 25, nameMatch);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void SetTypeHorseWindow(int type)
        {
            if (type == 0)
            {
                ChooseButton.Margin = new Thickness(0, 0, 0, 0);
                ChooseButton.Width = 0;
                ChooseButton.Visibility = Visibility.Hidden;
            }
            else if (type == 1)
            {
                ChooseButton.Margin = new Thickness(0, 0, 10, 0);
                ChooseButton.Width = 70;
                ChooseButton.Visibility = Visibility.Visible;

                EditButton.Margin = new Thickness(0, 0, 0, 0);
                EditButton.Width = 0;
                EditButton.Visibility = Visibility.Hidden;

                DeleteButton.Margin = new Thickness(0, 0, 0, 0);
                DeleteButton.Width = 0;
                DeleteButton.Visibility = Visibility.Hidden;
            }
        }

        private void ChooseButton_Click(object sender, RoutedEventArgs e)
        {
            int index = HorsesListBox.SelectedIndex;

            if (index == -1)
            {
                MessageBox.Show("Вы не выбрали коня");
                return;
            }

            try
            {
                HorseModel horseModel = myHorseModels[index];

                LessonWindow lessonWindow = this.Owner as LessonWindow;
                if (lessonWindow != null)
                    lessonWindow.GiveHorseModel(horseModel);

                this.Close();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show($"Неправильный индекс в списке: {index}");
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                HorsesListBox.Height = this.ActualHeight - 400;
            }
        }

        private void HorsesListBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.IsLoaded)
            {
                HorsesListBox.Height = this.ActualHeight - 400;
            }
        }
    }
}
