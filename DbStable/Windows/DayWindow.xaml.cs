﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DbStable.Models;

namespace DbStable.Windows
{
    public class Lesson
    {
        public LessonModel LessonModel { get; set; }
        public Button Button { get; set; }
    }

    public partial class DayWindow : Window
    {
        private List<HorseModel> myHorseModels = new List<HorseModel>();
        private List<TextBlock> dateTextBlocks = new List<TextBlock>();
        private List<Lesson[][]> lessons = new List<Lesson[][]>();

        private int amountDays;

        public DateTime ChoosedDay { get; set; }

        public DayWindow()
        {
            InitializeComponent();
        }

        public void CreatePage()
        {
            List<HorseModel> horseModels = Database.LoadAllHorses();

            if (horseModels.Count == 0)
            {
                MessageBox.Show("Для просмотра расписания необходимо добавить коней", "Ошибка");
                this.Close();
                return;
            }

            int count = horseModels.Count + 1;
            amountDays = 3;

            if (count * 40 < DaysScrollViewer.ActualHeight)
                amountDays = (int)(DaysScrollViewer.ActualHeight / (count * 40)) + 3;
            else
                amountDays = 3;

            for (int i = 0; i < amountDays; i++)
            {
                StackPanel dayStackPanel = new StackPanel();
                dayStackPanel.Margin = new Thickness(0, 40, 0, 0);
                Grid dayGrid = new Grid();
                for (int j = 0; j < count; j++)
                {
                    RowDefinition rowDefinition = new RowDefinition();
                    rowDefinition.MinHeight = 40;
                    dayGrid.RowDefinitions.Add(rowDefinition);
                }
                for (int j = 0; j < 14; j++)
                    dayGrid.ColumnDefinitions.Add(new ColumnDefinition());
                dayGrid.ColumnDefinitions[0].MinWidth = 140;
                DaysStackPanel.Children.Add(dayStackPanel);
                dayStackPanel.Children.Add(dayGrid);
            }

            DateTime currDate = ChoosedDay.AddDays(-(amountDays / 2 - 1));

            for (int i = 0; i < amountDays; i++)
            {
                StackPanel dayStackPanel = DaysStackPanel.Children[i] as StackPanel;
                if (dayStackPanel != null)
                {
                    Grid dayGrid = dayStackPanel.Children[0] as Grid;
                    if (dayGrid != null)
                    {
                        TextBlock dateTextBlock = new TextBlock();
                        dateTextBlock.Text = $"{currDate.ToString("dd.MM.yyyy")}";
                        dateTextBlock.FontSize = 13;
                        dateTextBlock.FontWeight = FontWeights.Bold;
                        dateTextBlock.TextWrapping = TextWrapping.Wrap;
                        dateTextBlocks.Add(dateTextBlock);
                        dayGrid.Children.Add(dateTextBlock);
                        Grid.SetRow(dateTextBlock, 0);
                        Grid.SetColumn(dateTextBlock, 0);

                        for (int j = 0; j < 13; j++)
                        {
                            TextBlock hourTextBlock = new TextBlock();
                            hourTextBlock.Text = $"{j + 8}";
                            hourTextBlock.FontSize = 13;
                            dayGrid.Children.Add(hourTextBlock);
                            Grid.SetRow(hourTextBlock, 0);
                            Grid.SetColumn(hourTextBlock, j + 1);
                        }
                    }
                }
                currDate = currDate.AddDays(1);
            }

            for (int i = 0; i < amountDays; i++)
            {
                Lesson[][] lessonArray = new Lesson[count - 1][];

                lessons.Add(lessonArray);

                StackPanel dayStackPanel = DaysStackPanel.Children[i] as StackPanel;
                if (dayStackPanel != null)
                {
                    Grid dayGrid = dayStackPanel.Children[0] as Grid;
                    if (dayGrid != null)
                    {
                        for (int j = 1; j < count; j++)
                        {
                            lessonArray[j - 1] = new Lesson[13];

                            TextBlock horseTextBlock = new TextBlock();
                            horseTextBlock.Text = $"{horseModels[j - 1].Name}";
                            horseTextBlock.TextWrapping = TextWrapping.Wrap;
                            dayGrid.Children.Add(horseTextBlock);
                            Grid.SetRow(horseTextBlock, j);
                            Grid.SetColumn(horseTextBlock, 0);

                            for (int k = 0; k < 13; k++)
                            {
                                Button lessonButton = new Button();
                                lessonButton.MinWidth = 120;
                                lessonButton.Content = "";
                                lessonButton.Click += HourButton_Click;
                                dayGrid.Children.Add(lessonButton);
                                Grid.SetRow(lessonButton, j);
                                Grid.SetColumn(lessonButton, k + 1);
                                
                                Lesson lesson = new Lesson();
                                lesson.LessonModel = null;
                                lesson.Button = lessonButton;

                                lessons[i][j - 1][k] = lesson;
                            }
                        }
                    }
                }
            }

            myHorseModels = horseModels;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CreatePage();

            for (int i = 0; i < dateTextBlocks.Count; i++)
            {
                DateTime dateTime = Convert.ToDateTime(dateTextBlocks[i].Text);
                ShowLessons(dateTime);
            }

            DaysScrollViewer.ScrollToVerticalOffset(DaysScrollViewer.ActualHeight / 2);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                int count = myHorseModels.Count + 1;
            }
        }

        private void DaysStackPanel_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (this.IsLoaded)
            {
                int count = myHorseModels.Count + 1;

                if (e.Delta > 0)
                {
                    DaysScrollViewer.ScrollToVerticalOffset(DaysScrollViewer.VerticalOffset - 40);
                }
                else if (e.Delta < 0)
                {
                    DaysScrollViewer.ScrollToVerticalOffset(DaysScrollViewer.VerticalOffset + 40);
                }
            }
        }

        private void DaysScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                double offset = DaysScrollViewer.VerticalOffset;

                if (offset >= DaysScrollViewer.ScrollableHeight - 1)
                {
                    int count = myHorseModels.Count + 1;

                    dateTextBlocks[0].Text = $"{ChoosedDay.AddDays(amountDays / 2 + 1).ToString("dd.MM.yyyy")}";
                    TextBlock dateTextBlock = dateTextBlocks[0];
                    dateTextBlocks.RemoveAt(0);
                    dateTextBlocks.Add(dateTextBlock);

                    StackPanel dayStackPanel = DaysStackPanel.Children[0] as StackPanel;
                    if (dayStackPanel != null)
                    {
                        DaysStackPanel.Children.RemoveAt(0);
                        DaysStackPanel.Children.Add(dayStackPanel);
                    }

                    ShowLessons(ChoosedDay.AddDays(amountDays / 2 + 1));

                    Lesson[][] tempLessons = lessons[0];
                    lessons.RemoveAt(0);
                    lessons.Add(tempLessons);

                    ChoosedDay = ChoosedDay.AddDays(1);

                    DaysScrollViewer.ScrollToVerticalOffset(DaysScrollViewer.VerticalOffset - 40 * count);
                }
                else if (offset - 1 <= 0)
                {
                    int count = myHorseModels.Count + 1;
                    int index = DaysStackPanel.Children.Count - 1;
                    int indexDates = dateTextBlocks.Count - 1;

                    dateTextBlocks[indexDates].Text = $"{ChoosedDay.AddDays(-amountDays / 2).ToString("dd.MM.yyyy")}";
                    TextBlock dateTextBlock = dateTextBlocks[indexDates];
                    dateTextBlocks.RemoveAt(indexDates);
                    dateTextBlocks.Insert(0, dateTextBlock);

                    StackPanel dayStackPanel = DaysStackPanel.Children[index] as StackPanel;
                    if (dayStackPanel != null)
                    {
                        DaysStackPanel.Children.RemoveAt(index);
                        DaysStackPanel.Children.Insert(0, dayStackPanel);
                    }

                    ShowLessons(ChoosedDay.AddDays(-amountDays / 2));

                    Lesson[][] tempLessons = lessons[indexDates];
                    lessons.RemoveAt(indexDates);
                    lessons.Insert(0, tempLessons);

                    ChoosedDay = ChoosedDay.AddDays(-1);

                    DaysScrollViewer.ScrollToVerticalOffset(40 * count);
                }
            }
        }

        public void ShowLessons(DateTime dateTime)
        {
            int day = -1;
            string date = dateTime.ToString("dd.MM.yyyy");
            for (int i = 0; i < dateTextBlocks.Count; i++)
            {
                if (dateTextBlocks[i].Text == date)
                {
                    day = i;
                    break;
                }
            }

            if (day != -1)
            {
                List<LessonModel> lessonModels = Database.LoadLessonsInDay(dateTime);

                for (int i = 0; i < myHorseModels.Count; i++)
                {
                    for (int j = 0; j < 13; j++)
                    {
                        lessons[day][i][j].LessonModel = null;
                        lessons[day][i][j].Button.Content = "";
                    }
                }

                foreach (LessonModel lessonModel in lessonModels)
                {
                    for (int j = 0; j < myHorseModels.Count; j++)
                    {
                        if (myHorseModels[j].Id == lessonModel.HorseId)
                        {
                            UserModel userModel = Database.LoadUser(lessonModel.LearnerId);
                            lessons[day][j][lessonModel.StartTime.Hour - 8].LessonModel = lessonModel;
                            lessons[day][j][lessonModel.StartTime.Hour - 8].Button.Content = $"{userModel.Name}";
                        }
                    }
                }
            }
        }

        private void HourButton_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (btn != null)
            {
                for (int i = 0; i < lessons.Count; i++)
                {
                    for (int j = 0; j < lessons[i].Length; j++)
                    {
                        for (int k = 0; k < lessons[i][j].Length; k++)
                        {
                            if (lessons[i][j][k].Button == btn)
                            {
                                LessonWindow lessonWindow = new LessonWindow();
                                lessonWindow.Owner = this;
                                if (lessons[i][j][k].LessonModel == null)
                                {
                                    lessonWindow.ChosenDate = Convert.ToDateTime(dateTextBlocks[i].Text);
                                    lessonWindow.ChosenHorse = myHorseModels[j];
                                    lessonWindow.ChosenHour = k + 8;
                                    lessonWindow.SetType(0);
                                }
                                else
                                {
                                    lessonWindow.CurrentLesson = lessons[i][j][k].LessonModel;
                                    lessonWindow.SetType(1);
                                }
                                lessonWindow.ShowDialog();
                            }
                        }
                    }
                }
            }
        }
    }
}
