﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class GroupsWindow : Window
    {
        private List<GroupModel> myGroupModels;

        public GroupsWindow()
        {
            InitializeComponent();
        }

        private void AddGroupButton_Click(object sender, RoutedEventArgs e)
        {
            string name = NewNameTextBox.Text;

            if (name.Length <= 0)
            {
                MessageBox.Show("Поле названия пусто");
                return;
            }
            else if (name.Length > 40)
            {
                MessageBox.Show("Название должно состоять не более, чем из 40 символов");
                return;
            }
            else if (Database.DoesGroupNameExist(name))
            {
                MessageBox.Show("Группа с таким названием уже существует");
                return;
            }

            GroupModel groupModel = new GroupModel();
            groupModel.Name = name;
            Database.CreateGroup(groupModel);
            groupModel.Id = Database.GetGroupIdByName(groupModel.Name);

            UpdatePage();

            MessageBox.Show($"Группа {groupModel.Name} успешно добавлена");
        }

        private void FindGroupButton_Click(object sender, RoutedEventArgs e)
        {
            UpdatePage();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            FoundNameTextBox.Text = "";

            UpdatePage();
        }

        private void EditGroupButton_Click(object sender, RoutedEventArgs e)
        {
            int index = GroupsListBox.SelectedIndex;

            try
            {
                GroupModel groupModel = myGroupModels[index];

                EditGroupWindow editGroupWindow = new EditGroupWindow();
                editGroupWindow.Owner = this;
                editGroupWindow.SetInfo(groupModel.Name);
                editGroupWindow.ShowDialog();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show($"Неправильный индекс в списке: {index}");
            }
        }

        private void DeleteGroupButton_Click(object sender, RoutedEventArgs e)
        {
            int index = GroupsListBox.SelectedIndex;

            try
            {
                GroupModel groupModel = myGroupModels[index];

                MessageBoxResult result = MessageBox.Show($"Вы уверены, что хотите удалить группу {groupModel.Name}?",
                    "Удаление группы", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.Yes)
                {
                    Database.DeleteGroup(groupModel.Id);
                    MessageBox.Show($"Вы удалили группу {groupModel.Name}");
                    UpdatePage();
                }
            }
            catch (ArgumentOutOfRangeException ex)
            {
                MessageBox.Show($"Неправильный индекс в списке: {index}");
            }
        }

        private void LoadGroupInfo(int page, int amount, string nameMatch = "")
        {
            int index = (page - 1) * amount + 1;

            GroupsListBox.Items.Clear();
            if (myGroupModels != null)
                myGroupModels.Clear();

            List<GroupModel> groupModels = Database.LoadGroups(25, index - 1, nameMatch);

            foreach (GroupModel groupModel in groupModels)
            {
                ListBoxItem item = new ListBoxItem();
                item.Content = $"{index} {groupModel.Name}";
                GroupsListBox.Items.Add(item);

                index++;
            }

            myGroupModels = groupModels;
        }

        private void PagesComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (PagesComboBox.SelectedIndex == -1)
                PagesComboBox.SelectedIndex = PagesComboBox.Items.Count - 1;

            UpdatePage();
        }

        public void UpdatePage()
        {
            string nameMatch = FoundNameTextBox.Text;
            ComboBoxItem item;
            int rows = Database.GetCountGroups(nameMatch);
            int pages = (rows - 1) / 25 + 1;
            int count = PagesComboBox.Items.Count;

            if (pages < 0)
                pages = 0;

            if (pages > count)
            {
                for (int i = 0; i < pages - count; i++)
                {
                    item = new ComboBoxItem();
                    item.Content = PagesComboBox.Items.Count + 1;
                    PagesComboBox.Items.Add(item);
                }
            }
            else if (pages < count)
            {
                while (pages != PagesComboBox.Items.Count && PagesComboBox.Items.Count > 0)
                    PagesComboBox.Items.RemoveAt(PagesComboBox.Items.Count - 1);
            }

            int page;
            item = PagesComboBox.SelectedItem as ComboBoxItem;
            if (item != null)
            {
                int.TryParse(Convert.ToString(item.Content), out page);
                LoadGroupInfo(page, 25, nameMatch);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.IsLoaded)
            {
                GroupsListBox.Height = this.ActualHeight - 400;
            }
        }

        private void GroupsListBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.IsLoaded)
            {
                GroupsListBox.Height = this.ActualHeight - 400;
            }
        }
    }
}
