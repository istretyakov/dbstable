﻿using System.Windows;
using DbStable.Models;

namespace DbStable.Windows
{
    public partial class EditHorseWindow : Window
    {
        private HorseModel myHorseModel;

        public EditHorseWindow()
        {
            InitializeComponent();
        }

        private void SaveHorseButton_Click(object sender, RoutedEventArgs e)
        {
            string name = NameTextBox.Text;

            myHorseModel.Name = name;
            Database.SaveHorse(myHorseModel);

            MessageBox.Show($"Вы сохранили коня {myHorseModel.Name}");

            HorsesWindow horsesWindow = this.Owner as HorsesWindow;

            this.Close();

            if (horsesWindow != null)
                horsesWindow.UpdatePage();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void SetInfo(string name)
        {
            HorseModel horseModel = Database.LoadHorse(name);

            NameTextBox.Text = horseModel.Name;

            myHorseModel = horseModel;
        }
    }
}
