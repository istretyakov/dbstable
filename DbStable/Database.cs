﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using DbStable.Models;

namespace DbStable
{
    public class Database
    {
        private static string connectionInfo = "Data Source=stable.db;";

        public static void CreateUser(UserModel userModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"INSERT INTO `users` (`name`, `group`, `age`, `number`) VALUES (" +
                        $"'{userModel.Name}', " +
                        $"'{userModel.Group}', " +
                        $"'{userModel.Age}', " +
                        $"'{userModel.Number}')";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static UserModel LoadUser(int dbId)
        {
            UserModel userModel = new UserModel();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `users` WHERE `id` = '{dbId}' LIMIT 1";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            object tmp;
                            tmp = reader["id"];
                            userModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            userModel.Name = Convert.ToString(tmp);
                            tmp = reader["group"];
                            userModel.Group = Convert.ToInt32(tmp);
                            tmp = reader["age"];
                            userModel.Age = Convert.ToInt32(tmp);
                            tmp = reader["number"];
                            userModel.Number = Convert.ToString(tmp);
                        }
                    }
                }
            }

            return userModel;
        }

        public static UserModel LoadUser(string name)
        {
            UserModel userModel = new UserModel();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `users` WHERE `name` = '{name}' LIMIT 1";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            object tmp;
                            tmp = reader["id"];
                            userModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            userModel.Name = Convert.ToString(tmp);
                            tmp = reader["group"];
                            userModel.Group = Convert.ToInt32(tmp);
                            tmp = reader["age"];
                            userModel.Age = Convert.ToInt32(tmp);
                            tmp = reader["number"];
                            userModel.Number = Convert.ToString(tmp);
                        }
                    }
                }
            }

            return userModel;
        }

        public static List<UserModel> LoadUsers(int amount, int startLimit = 0, int group = 0, string nameMatch = "")
        {
            List<UserModel> userModels = new List<UserModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM `users` WHERE ";
                    if (group != 0)
                        cmd.CommandText += $"`group` = '{group}'";
                    if (nameMatch != String.Empty)
                    {
                        if (group != 0)
                            cmd.CommandText += " AND ";
                        cmd.CommandText += $"`name` LIKE '%{nameMatch}%'";
                    }
                    if (group == 0 && nameMatch == String.Empty)
                        cmd.CommandText += "1";
                    cmd.CommandText += $" ORDER BY `id` ASC LIMIT {startLimit}, {amount}";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UserModel userModel = new UserModel();
                            object tmp;
                            tmp = reader["id"];
                            userModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            userModel.Name = Convert.ToString(tmp);
                            tmp = reader["group"];
                            userModel.Group = Convert.ToInt32(tmp);
                            tmp = reader["age"];
                            userModel.Age = Convert.ToInt32(tmp);
                            tmp = reader["number"];
                            userModel.Number = Convert.ToString(tmp);
                            userModels.Add(userModel);
                        }
                    }
                }
            }

            return userModels;
        }

        public static void SaveUser(UserModel userModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"UPDATE `users` SET " +
                        $"`name` = '{userModel.Name}', " +
                        $"`group` = '{userModel.Group}', " +
                        $"`age` = '{userModel.Age}', " +
                        $"`number` = '{userModel.Number}' " +
                        $"WHERE `id` = '{userModel.Id}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DeleteUser(int id)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"DELETE FROM `users` WHERE `id` = {id}";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static int GetUserIdByName(string name)
        {
            int id = 0;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT `id` FROM `users` WHERE `name` = '{name}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            object tmp = reader["id"];
                            id = Convert.ToInt32(tmp);
                        }
                    }
                }
            }

            return id;
        }

        public static int GetCountUsers(int group = 0, string nameMatch = "")
        {
            int rows = 0;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT COUNT(*) FROM `users` WHERE ";
                    if (group != 0)
                        cmd.CommandText += $"`group` = '{group}'";
                    if (nameMatch != String.Empty)
                    {
                        if (group != 0)
                            cmd.CommandText += " AND ";
                        cmd.CommandText += $"`name` LIKE '%{nameMatch}%'";
                    }
                    if (group == 0 && nameMatch == String.Empty)
                        cmd.CommandText += "1";

                    rows = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }

            return rows;
        }

        public static bool DoesUserNameExist(string name)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT `id` FROM `users` WHERE `name` = '{name}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            return true;
                    }
                }
            }

            return false;
        }

        public static void CreateHorse(HorseModel horseModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"INSERT INTO `horses` (`name`) VALUES ('{horseModel.Name}')";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static HorseModel LoadHorse(string name)
        {
            HorseModel horseModel = new HorseModel();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `horses` WHERE `name` = '{name}' LIMIT 1";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            object tmp;
                            tmp = reader["id"];
                            horseModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            horseModel.Name = Convert.ToString(tmp);
                        }
                    }
                }
            }

            return horseModel;
        }

        public static HorseModel LoadHorse(int dbId)
        {
            HorseModel horseModel = new HorseModel();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `horses` WHERE `id` = '{dbId}' LIMIT 1";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            object tmp;
                            tmp = reader["id"];
                            horseModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            horseModel.Name = Convert.ToString(tmp);
                        }
                    }
                }
            }

            return horseModel;
        }

        public static List<HorseModel> LoadAllHorses()
        {
            List<HorseModel> horseModels = new List<HorseModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `horses` ORDER BY `id` ASC";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            HorseModel horseModel = new HorseModel();
                            object tmp;
                            tmp = reader["id"];
                            horseModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            horseModel.Name = Convert.ToString(tmp);
                            horseModels.Add(horseModel);
                        }
                    }
                }
            }

            return horseModels;
        }

        public static List<HorseModel> LoadHorses(int amount, int startLimit = 0, string nameMatch = "")
        {
            List<HorseModel> horseModels = new List<HorseModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `horses` WHERE ";
                    if (nameMatch != String.Empty)
                        cmd.CommandText += $"`name` LIKE '%{nameMatch}%' ";
                    else
                        cmd.CommandText += "1 ";
                    cmd.CommandText += $"ORDER BY `id` ASC LIMIT {startLimit}, {amount}";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            HorseModel horseModel = new HorseModel();
                            object tmp;
                            tmp = reader["id"];
                            horseModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            horseModel.Name = Convert.ToString(tmp);
                            horseModels.Add(horseModel);
                        }
                    }
                }
            }

            return horseModels;
        }

        public static void SaveHorse(HorseModel horseModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"UPDATE `horses` SET " +
                        $"`name` = '{horseModel.Name}' " +
                        $"WHERE `id` = '{horseModel.Id}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DeleteHorse(int id)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"DELETE FROM `horses` WHERE `id` = {id}";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static int GetHorseIdByName(string name)
        {
            int id = 0;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT `id` FROM `horses` WHERE `name` = '{name}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            object tmp = reader["id"];
                            id = Convert.ToInt32(tmp);
                        }
                    }
                }
            }

            return id;
        }

        public static int GetCountHorses(string nameMatch = "")
        {
            int rows = 0;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT COUNT(*) FROM `horses` WHERE ";
                    if (nameMatch != String.Empty)
                        cmd.CommandText += $"`name` LIKE '%{nameMatch}%'";
                    else
                        cmd.CommandText += "1";
                    rows = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }

            return rows;
        }

        public static bool DoesHorseNameExist(string name)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT `id` FROM `horses` WHERE `name` = '{name}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            return true;
                    }
                }
            }

            return false;
        }

        public static void CreateGroup(GroupModel groupModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"INSERT INTO `groups` (`name`) VALUES ('{groupModel.Name}')";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static GroupModel LoadGroup(string name)
        {
            GroupModel groupModel = new GroupModel();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `groups` WHERE `name` = '{name}' LIMIT 1";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            object tmp;
                            tmp = reader["id"];
                            groupModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            groupModel.Name = Convert.ToString(tmp);
                        }
                    }
                }
            }

            return groupModel;
        }

        public static List<GroupModel> LoadGroups(int amount, int startLimit = 0, string nameMatch = "")
        {
            List<GroupModel> groupModels = new List<GroupModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM `groups` WHERE ";
                    if (nameMatch != String.Empty)
                        cmd.CommandText += $"`name` LIKE '%{nameMatch}%' ";
                    else
                        cmd.CommandText += "1 ";
                    cmd.CommandText += $"ORDER BY `id` ASC LIMIT {startLimit}, {amount}";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GroupModel groupModel = new GroupModel();
                            object tmp;
                            tmp = reader["id"];
                            groupModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            groupModel.Name = Convert.ToString(tmp);
                            groupModels.Add(groupModel);
                        }
                    }
                }
            }

            return groupModels;
        }

        public static List<GroupModel> LoadAllGroups()
        {
            List<GroupModel> groupModels = new List<GroupModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `groups` ORDER BY `id` ASC";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GroupModel groupModel = new GroupModel();
                            object tmp;
                            tmp = reader["id"];
                            groupModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["name"];
                            groupModel.Name = Convert.ToString(tmp);
                            groupModels.Add(groupModel);
                        }
                    }
                }
            }

            return groupModels;
        }

        public static void SaveGroup(GroupModel groupModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"UPDATE `groups` SET " +
                        $"`name` = '{groupModel.Name}' " +
                        $"WHERE `id` = '{groupModel.Id}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DeleteGroup(int id)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"DELETE FROM `groups` WHERE `id` = {id}";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static int GetGroupIdByName(string name)
        {
            int id = 0;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT `id` FROM `groups` WHERE `name` = '{name}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            object tmp = reader["id"];
                            id = Convert.ToInt32(tmp);
                        }
                    }
                }
            }

            return id;
        }

        public static int GetCountGroups(string nameMatch = "")
        {
            int rows = 0;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) FROM `groups` WHERE ";
                    if (nameMatch != String.Empty)
                        cmd.CommandText += $"`name` LIKE '%{nameMatch}%'";
                    else
                        cmd.CommandText += "1";
                    rows = Convert.ToInt32(cmd.ExecuteScalar());
                }
            }

            return rows;
        }

        public static bool DoesGroupNameExist(string name)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT `id` FROM `groups` WHERE `name` = '{name}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            return true;
                    }
                }
            }

            return false;
        }

        public static void CreateLesson(LessonModel lessonModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"INSERT INTO `lessons` " +
                        $"(`coach_id`, `learner_id`, `horse_id`, `starttime`, `finishtime`, `type`, `income_id`, `salary_id`) VALUES " +
                        $"('{lessonModel.CoachId}', " +
                        $"'{lessonModel.LearnerId}', " +
                        $"'{lessonModel.HorseId}', " +
                        $"'{lessonModel.StartTime.ToString("yyyy-MM-dd HH:mm:ss")}', " +
                        $"'{lessonModel.FinishTime.ToString("yyyy-MM-dd HH:mm:ss")}', " +
                        $"'{lessonModel.Type}', " +
                        $"'{lessonModel.IncomeId}', " +
                        $"'{lessonModel.SalaryId}')";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<LessonModel> LoadLessonsInDay(DateTime dateTime)
        {
            List<LessonModel> lessonModels = new List<LessonModel>();
            DateTime tomorrowDate = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day).AddDays(1);
            DateTime yesterdayDate = tomorrowDate.AddDays(-1);

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `lessons` WHERE " +
                        $"`starttime` < '{tomorrowDate.ToString("yyyy-MM-dd HH:mm:ss")}' AND " +
                        $"`starttime` >= '{yesterdayDate.ToString("yyyy-MM-dd HH:mm:ss")}'";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LessonModel lessonModel = new LessonModel();
                            object tmp;
                            tmp = reader["id"];
                            lessonModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["coach_id"];
                            lessonModel.CoachId = Convert.ToInt32(tmp);
                            tmp = reader["learner_id"];
                            lessonModel.LearnerId = Convert.ToInt32(tmp);
                            tmp = reader["horse_id"];
                            lessonModel.HorseId = Convert.ToInt32(tmp);
                            tmp = reader["starttime"];
                            lessonModel.StartTime = Convert.ToDateTime(tmp);
                            tmp = reader["finishtime"];
                            lessonModel.FinishTime = Convert.ToDateTime(tmp);
                            tmp = reader["type"];
                            lessonModel.Type = Convert.ToInt32(tmp);
                            tmp = reader["income_id"];
                            lessonModel.IncomeId = Convert.ToInt32(tmp);
                            tmp = reader["salary_id"];
                            lessonModel.SalaryId = Convert.ToInt32(tmp);
                            lessonModels.Add(lessonModel);
                        }
                    }
                }
            }

            return lessonModels;
        }

        public static LessonModel LoadLesson(int dbId)
        {
            LessonModel lessonModel = null;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `lessons` WHERE `id` = '{dbId}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            lessonModel = new LessonModel();
                            object tmp;
                            tmp = reader["id"];
                            lessonModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["coach_id"];
                            lessonModel.CoachId = Convert.ToInt32(tmp);
                            tmp = reader["learner_id"];
                            lessonModel.LearnerId = Convert.ToInt32(tmp);
                            tmp = reader["horse_id"];
                            lessonModel.HorseId = Convert.ToInt32(tmp);
                            tmp = reader["starttime"];
                            lessonModel.StartTime = Convert.ToDateTime(tmp);
                            tmp = reader["finishtime"];
                            lessonModel.FinishTime = Convert.ToDateTime(tmp);
                            tmp = reader["type"];
                            lessonModel.Type = Convert.ToInt32(tmp);
                            tmp = reader["income_id"];
                            lessonModel.IncomeId = Convert.ToInt32(tmp);
                            tmp = reader["salary_id"];
                            lessonModel.SalaryId = Convert.ToInt32(tmp);
                        }
                    }
                }
            }

            return lessonModel;
        }

        public static List<LessonModel> LoadLessons(int userId, DateTime dateTime, int limit)
        {
            List<LessonModel> lessonModels = new List<LessonModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `lessons` WHERE " +
                        $"`starttime` > '{dateTime.ToString("yyyy-MM-dd HH:mm:ss")}' AND " +
                        $"`learner_id` = '{userId}' " +
                        $"LIMIT {limit}";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            LessonModel lessonModel = new LessonModel();
                            object tmp;
                            tmp = reader["id"];
                            lessonModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["coach_id"];
                            lessonModel.CoachId = Convert.ToInt32(tmp);
                            tmp = reader["learner_id"];
                            lessonModel.LearnerId = Convert.ToInt32(tmp);
                            tmp = reader["horse_id"];
                            lessonModel.HorseId = Convert.ToInt32(tmp);
                            tmp = reader["starttime"];
                            lessonModel.StartTime = Convert.ToDateTime(tmp);
                            tmp = reader["finishtime"];
                            lessonModel.FinishTime = Convert.ToDateTime(tmp);
                            tmp = reader["type"];
                            lessonModel.Type = Convert.ToInt32(tmp);
                            tmp = reader["income_id"];
                            lessonModel.IncomeId = Convert.ToInt32(tmp);
                            tmp = reader["salary_id"];
                            lessonModel.SalaryId = Convert.ToInt32(tmp);
                            lessonModels.Add(lessonModel);
                        }
                    }
                }
            }

            return lessonModels;
        }

        public static void SaveLesson(LessonModel lessonModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"UPDATE `lessons` SET " +
                        $"`coach_id` = '{lessonModel.CoachId}', " +
                        $"`learner_id` = '{lessonModel.LearnerId}', " +
                        $"`horse_id` = '{lessonModel.HorseId}', " +
                        $"`starttime` = '{lessonModel.StartTime.ToString("yyyy-MM-dd HH:mm:ss")}', " +
                        $"`finishtime` = '{lessonModel.FinishTime.ToString("yyyy-MM-dd HH:mm:ss")}', " +
                        $"`type` = '{lessonModel.Type}', " +
                        $"`income_id` = '{lessonModel.IncomeId}', " +
                        $"`salary_id` = '{lessonModel.SalaryId}' " +
                        $"WHERE `id` = '{lessonModel.Id}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DeleteLesson(int dbId)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"DELETE FROM `lessons` WHERE `id` = '{dbId}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static bool DoesLessonExist(int horseId, DateTime dateTime)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT `id` FROM `lessons` WHERE " +
                        $"`starttime` = '{dateTime.ToString("yyyy-MM-dd HH:mm:ss")}' AND" +
                        $"`horse_id` = '{horseId}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                            return true;
                    }
                }
            }

            return false;
        }

        public static void CreatePayment(PaymentModel paymentModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"INSERT INTO `payments` (`date`, `value`, `user_id`, `type`) VALUES " +
                        $"('{paymentModel.Date.ToString("yyyy-MM-dd HH:mm:ss")}', " +
                        $"'{paymentModel.Value}', " +
                        $"'{paymentModel.UserId}', " +
                        $"'{(int)paymentModel.Type}')";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static PaymentModel LoadPayment(int dbId)
        {
            PaymentModel paymentModel = null;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `payments` WHERE `id` = '{dbId}' LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            paymentModel = new PaymentModel();
                            object tmp;
                            tmp = reader["id"];
                            paymentModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["date"];
                            paymentModel.Date = Convert.ToDateTime(tmp);
                            tmp = reader["value"];
                            paymentModel.Value = Convert.ToInt32(tmp);
                            tmp = reader["user_id"];
                            paymentModel.UserId = Convert.ToInt32(tmp);
                            tmp = reader["type"];
                            paymentModel.Type = (PaymentTypes)Convert.ToInt32(tmp);
                        }
                    }
                }
            }

            return paymentModel;
        }

        public static List<PaymentModel> LoadPaymentsForDay(DateTime dateTime)
        {
            List<PaymentModel> paymentModels = new List<PaymentModel>();

            DateTime todayDate = dateTime.AddHours(-dateTime.Hour);
            todayDate = todayDate.AddMinutes(-todayDate.Minute);
            todayDate = todayDate.AddSeconds(-todayDate.Second);
            DateTime tomorrowDate = dateTime.AddDays(1);
            tomorrowDate = tomorrowDate.AddHours(-tomorrowDate.Hour);
            tomorrowDate = tomorrowDate.AddMinutes(-tomorrowDate.Minute);
            tomorrowDate = tomorrowDate.AddSeconds(-tomorrowDate.Second);

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `payments` WHERE " +
                        $"`date` >= '{todayDate.ToString("yyyy-MM-dd HH:mm:ss")}' AND" +
                        $"`date` <= '{tomorrowDate.ToString("yyyy-MM-dd HH:mm:ss")}'";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PaymentModel paymentModel = new PaymentModel();
                            object tmp;
                            tmp = reader["id"];
                            paymentModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["date"];
                            paymentModel.Date = Convert.ToDateTime(tmp);
                            tmp = reader["value"];
                            paymentModel.Value = Convert.ToInt32(tmp);
                            tmp = reader["user_id"];
                            paymentModel.UserId = Convert.ToInt32(tmp);
                            tmp = reader["type"];
                            paymentModel.Type = (PaymentTypes)Convert.ToInt32(tmp);
                            paymentModels.Add(paymentModel);
                        }
                    }
                }
            }

            return paymentModels;
        }

        public static List<PaymentModel> LoadPaymentsForDates(DateTime startDateTime, DateTime finishDateTime)
        {
            List<PaymentModel> paymentModels = new List<PaymentModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `payments` WHERE " +
                        $"`date` >= '{startDateTime.ToString("yyyy-MM-dd HH:mm:ss")}' AND" +
                        $"`date` <= '{finishDateTime.ToString("yyyy-MM-dd HH:mm:ss")}'";

                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            PaymentModel paymentModel = new PaymentModel();
                            object tmp;
                            tmp = reader["id"];
                            paymentModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["date"];
                            paymentModel.Date = Convert.ToDateTime(tmp);
                            tmp = reader["value"];
                            paymentModel.Value = Convert.ToInt32(tmp);
                            tmp = reader["user_id"];
                            paymentModel.UserId = Convert.ToInt32(tmp);
                            tmp = reader["type"];
                            paymentModel.Type = (PaymentTypes)Convert.ToInt32(tmp);
                            paymentModels.Add(paymentModel);
                        }
                    }
                }
            }

            return paymentModels;
        }

        public static PaymentModel LoadLastPayment()
        {
            PaymentModel paymentModel = null;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `payments` ORDER BY `id` DESC LIMIT 1";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            paymentModel = new PaymentModel();
                            object tmp;
                            tmp = reader["id"];
                            paymentModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["date"];
                            paymentModel.Date = Convert.ToDateTime(tmp);
                            tmp = reader["value"];
                            paymentModel.Value = Convert.ToInt32(tmp);
                            tmp = reader["user_id"];
                            paymentModel.UserId = Convert.ToInt32(tmp);
                            tmp = reader["type"];
                            paymentModel.Type = (PaymentTypes)Convert.ToInt32(tmp);
                        }
                    }
                }
            }

            return paymentModel;
        }

        public static void SavePayment(PaymentModel paymentModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"UPDATE `payments` SET " +
                        $"`date` = '{paymentModel.Date.ToString("yyyy-MM-dd HH:mm:ss")}', " +
                        $"`value` = '{paymentModel.Value}', " +
                        $"`user_id` = '{paymentModel.UserId}', " +
                        $"`type` = '{(int)paymentModel.Type}' " +
                        $"WHERE `id` = '{paymentModel.Id}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DeletePayment(int dbId)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"DELETE FROM `payments` WHERE `id` = '{dbId}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void CreateExpense(ExpenseModel expenseModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"INSERT INTO `Expenses` " +
                        $"(`Comment`, `Amount`, `Date`, `PaymentId`) VALUES " +
                        $"('{expenseModel.Comment}', " +
                        $"'{expenseModel.Amount}', " +
                        $"'{expenseModel.Date.ToString("yyyy-MM-dd")}', " +
                        $"'{expenseModel.PaymentId}')";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<ExpenseModel> LoadExpenses(int amount, int start)
        {
            List<ExpenseModel> expenseModels = new List<ExpenseModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `Expenses` " +
                        $"ORDER BY `Id` ASC " +
                        $"LIMIT {start}, {amount}";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ExpenseModel expenseModel = new ExpenseModel();
                            object tmp;

                            tmp = reader["Id"];
                            expenseModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["Comment"];
                            expenseModel.Comment = Convert.ToString(tmp);
                            tmp = reader["Amount"];
                            expenseModel.Amount = Convert.ToInt32(tmp);
                            tmp = reader["Date"];
                            expenseModel.Date = Convert.ToDateTime(tmp);
                            tmp = reader["PaymentId"];
                            expenseModel.PaymentId = Convert.ToInt32(tmp);

                            expenseModels.Add(expenseModel);
                        }
                    }
                }
            }

            return expenseModels;
        }

        public static void SaveExpense(ExpenseModel expenseModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"UPDATE `Expenses` SET " +
                        $"`Comment` = '{expenseModel.Comment}', " +
                        $"`Amount` = '{expenseModel.Amount}', " +
                        $"`Date` = '{expenseModel.Date.ToString("yyyy-MM-dd")}', " +
                        $"`PaymentId` = '{expenseModel.Id}' " +
                        $"WHERE `Id` = '{expenseModel.Id}'";
                }
            }
        }

        public static void DeleteExpenses(ExpenseModel expenseModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"DELETE FROM `Expenses` WHERE `Id` = '{expenseModel.Id}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static int GetCountExpenses()
        {
            int count = 0;

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT COUNT() FROM `Expenses`";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            object tmp;
                            tmp = reader["COUNT()"];
                            count = Convert.ToInt32(tmp);
                        }
                    }
                }
            }

            return count;
        }

        public static void CreateSeasonTicket(SeasonTicketModel seasonTicketModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"INSERT INTO `SeasonTickets` " +
                        $"(`UserId`, `StartTime`, `FinishTime`, `PaymentId`, `LastLessons`, `MaxLessons`) VALUES " +
                        $"('{seasonTicketModel.UserId}', " +
                        $"'{seasonTicketModel.StartTime.ToString("yyyy-MM-dd")}', " +
                        $"'{seasonTicketModel.FinishTime.ToString("yyyy-MM-dd")}', " +
                        $"'{seasonTicketModel.PaymentId}', " +
                        $"'{seasonTicketModel.LastLessons}', " +
                        $"'{seasonTicketModel.MaxLessons}')";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static List<SeasonTicketModel> LoadSeasonTickets(int userId, int limit)
        {
            List<SeasonTicketModel> seasonTicketModels = new List<SeasonTicketModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `SeasonTickets` WHERE `UserId` = '{userId}' LIMIT {limit}";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            SeasonTicketModel seasonTicketModel = new SeasonTicketModel();
                            object tmp;

                            tmp = reader["Id"];
                            seasonTicketModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["UserId"];
                            seasonTicketModel.UserId = Convert.ToInt32(tmp);
                            tmp = reader["StartTime"];
                            seasonTicketModel.StartTime = Convert.ToDateTime(tmp);
                            tmp = reader["FinishTime"];
                            seasonTicketModel.FinishTime = Convert.ToDateTime(tmp);
                            tmp = reader["PaymentId"];
                            seasonTicketModel.PaymentId = Convert.ToInt32(tmp);
                            tmp = reader["LastLessons"];
                            seasonTicketModel.LastLessons = Convert.ToInt32(tmp);
                            tmp = reader["MaxLessons"];
                            seasonTicketModel.MaxLessons = Convert.ToInt32(tmp);

                            seasonTicketModels.Add(seasonTicketModel);
                        }
                    }
                }
            }

            return seasonTicketModels;
        }

        public static List<SeasonTicketModel> LoadLastSeasonTickets(int userId, int amount)
        {
            List<SeasonTicketModel> seasonTicketModels = new List<SeasonTicketModel>();

            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"SELECT * FROM `SeasonTickets` WHERE `UserId` = '{userId}' LIMIT {amount}";
                    using (SQLiteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            SeasonTicketModel seasonTicketModel = new SeasonTicketModel();
                            object tmp;

                            tmp = reader["Id"];
                            seasonTicketModel.Id = Convert.ToInt32(tmp);
                            tmp = reader["UserId"];
                            seasonTicketModel.UserId = Convert.ToInt32(tmp);
                            tmp = reader["StartTime"];
                            seasonTicketModel.StartTime = Convert.ToDateTime(tmp);
                            tmp = reader["FinishTime"];
                            seasonTicketModel.FinishTime = Convert.ToDateTime(tmp);
                            tmp = reader["PaymentId"];
                            seasonTicketModel.PaymentId = Convert.ToInt32(tmp);
                            tmp = reader["LastLessons"];
                            seasonTicketModel.LastLessons = Convert.ToInt32(tmp);
                            tmp = reader["MaxLessons"];
                            seasonTicketModel.MaxLessons = Convert.ToInt32(tmp);

                            seasonTicketModels.Add(seasonTicketModel);
                        }
                    }
                }
            }

            return seasonTicketModels;
        }

        public static void SaveSeasonTicket(SeasonTicketModel seasonTicketModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"UPDATE `SeasonTickets` SET " +
                        $"`UserId` = '{seasonTicketModel.UserId}', " +
                        $"`StartTime` = '{seasonTicketModel.StartTime.ToString("yyyy-MM-dd")}', " +
                        $"`FinishTime` = '{seasonTicketModel.FinishTime.ToString("yyyy-MM-dd")}', " +
                        $"`PaymentId` = '{seasonTicketModel.PaymentId}', " +
                        $"`LastLessons` = '{seasonTicketModel.LastLessons}', " +
                        $"`MaxLessons` = '{seasonTicketModel.MaxLessons}' " +
                        $"WHERE `Id` = '{seasonTicketModel.Id}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public static void DeleteSeasonTicket(SeasonTicketModel seasonTicketModel)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionInfo))
            {
                conn.Open();

                using (SQLiteCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = $"DELETE FROM `SeasonTickets` WHERE `Id` = '{seasonTicketModel.Id}'";
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
