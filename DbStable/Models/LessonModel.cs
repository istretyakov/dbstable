﻿using System;

namespace DbStable.Models
{
    public class LessonModel
    {
        public int Id { get; set; }
        public int CoachId { get; set; }
        public int LearnerId { get; set; }
        public int HorseId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public int Type { get; set; }
        public int IncomeId { get; set; }
        public int SalaryId { get; set; }
    }
}
