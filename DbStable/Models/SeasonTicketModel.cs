﻿using System;

namespace DbStable.Models
{
    public class SeasonTicketModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public int PaymentId { get; set; }
        public int LastLessons { get; set; }
        public int MaxLessons { get; set; }
    }
}
