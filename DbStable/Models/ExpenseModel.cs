﻿using System;

namespace DbStable.Models
{
    public class ExpenseModel
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public int Amount { get; set; }
        public DateTime Date { get; set; }
        public int PaymentId { get; set; }
    }
}
