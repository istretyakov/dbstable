﻿using System;

namespace DbStable.Models
{
    public enum PaymentTypes
    {
        SeasonTicket = 0,
        OneTime,
        Salary,
        Other,
        NotPaid,
        Ticket
    }

    public class PaymentModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Value { get; set; }
        public int UserId { get; set; }
        public PaymentTypes Type { get; set; }
    }
}
