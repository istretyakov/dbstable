﻿namespace DbStable.Classes
{
    public class MoneyStats
    {
        public int OneTimePaid { get; set; }
        public int SeasonTicket { get; set; }
        public int Ticket { get; set; }
        public int Salary { get; set; }
        public int OtherPlus { get; set; }
        public int OtherMinus { get; set; }
        public int NotPaid { get; set; }

        public MoneyStats()
        {
            OneTimePaid = 0;
            SeasonTicket = 0;
            Ticket = 0;
            Salary = 0;
            OtherPlus = 0;
            OtherMinus = 0;
            NotPaid = 0;
        }
    }
}
